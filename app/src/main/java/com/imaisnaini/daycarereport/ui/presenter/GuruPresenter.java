package com.imaisnaini.daycarereport.ui.presenter;

import android.content.Context;

import com.imaisnaini.daycarereport.bl.db.dao.GuruDao;
import com.imaisnaini.daycarereport.bl.db.dao.OrtuDao;
import com.imaisnaini.daycarereport.bl.db.model.Guru;
import com.imaisnaini.daycarereport.bl.db.model.Ortu;
import com.imaisnaini.daycarereport.ui.view.GuruView;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class GuruPresenter {
    Context context;
    private GuruView guruView;

    public GuruPresenter(Context context, GuruView guruView) {
        this.context = context;
        this.guruView = guruView;
    }

    public void load(){
        guruView.showLoading();
        List<Guru> list = new ArrayList<>();
        try {
            list = GuruDao.getGuruDao().read();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        guruView.loadGuru(list);
        guruView.hideLoading();
    }

    public Guru getByID(int id){
        Guru guru = null;
        try {
            guru = GuruDao.getGuruDao().getByID(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return guru;
    }
}
