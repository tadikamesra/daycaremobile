package com.imaisnaini.daycarereport.ui.fragment;


import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.MaterialDialog;
import com.imaisnaini.daycarereport.R;
import com.imaisnaini.daycarereport.bl.db.model.Murid;
import com.imaisnaini.daycarereport.event.MuridTabEvent;
import com.imaisnaini.daycarereport.ui.activity.DetailOrtuActivity;
import com.imaisnaini.daycarereport.ui.adapter.MuridAdapter;
import com.imaisnaini.daycarereport.ui.adapter.UmurMuridAdapter;
import com.imaisnaini.daycarereport.ui.dialog.DialogBuilder;
import com.imaisnaini.daycarereport.ui.presenter.MuridPresenter;
import com.imaisnaini.daycarereport.ui.view.MuridView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class MuridsFragment extends Fragment implements MuridView {
    @BindView(R.id.rvMurid_murid)
    RecyclerView rvMurid;
    @BindView(R.id.rvUmur_murid)
    RecyclerView rvUmur;

    private MaterialDialog dialog;
    private int tab;
    private MuridPresenter muridPresenter;
    private MuridAdapter muridAdapter;
    private UmurMuridAdapter umurAdapter;
    private List<String> listUmur = new ArrayList<>();
    private List<Murid> listMurid = new ArrayList<>();

    public MuridsFragment() {
        // Required empty public constructor
    }

    @Subscribe(threadMode = ThreadMode.MAIN) public void setUmur(MuridTabEvent event){
        tab = event.idTab;
        //loadUmur(tab);
        loadMurid(tab);
        //Log.i("USIA_MODE", "setMode: " + tab);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_murids, container, false);
        ButterKnife.bind(this, view);
        init();
        EventBus.getDefault().register(this);
        return view;
    }

    private void init(){
        muridPresenter = new MuridPresenter(getContext(),this);
        muridAdapter = new MuridAdapter(getContext());
        umurAdapter = new UmurMuridAdapter(getContext());

        rvUmur.setHasFixedSize(true);
        rvUmur.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false));
        rvUmur.setAdapter(umurAdapter);

        rvMurid.setHasFixedSize(true);
        rvMurid.setLayoutManager(new LinearLayoutManager(getContext()));
        rvMurid.setAdapter(muridAdapter);

    }

    private void loadMurid(int i){
        listMurid.clear();
        if (i == 0){
            listMurid = muridPresenter.loadByAge(0, 12);
        }else if (i == 1){
            listMurid = muridPresenter.loadByAge(13, 24);
        }else if (i == 2){
            listMurid = muridPresenter.loadByAge(25, 48);
        }else {
            listMurid = muridPresenter.loadByAge(49, 72);
        }
        muridAdapter.generate(listMurid);
        muridAdapter.notifyDataSetChanged();
    }

    private void loadUmur(int i){
        listUmur.clear();
        String[] array = null;
        if (i == 0)
            array = getResources().getStringArray(R.array.umur1);
        else if (i == 1)
            array = getResources().getStringArray(R.array.umur2);
        else if (i == 2)
            array = getResources().getStringArray(R.array.umur3);
        else
            array = getResources().getStringArray(R.array.umur4);

        for (String item :array) {
            listUmur.add(item);
        }
        umurAdapter.generate(listUmur);
        umurAdapter.notifyDataSetChanged();
    }

    @Override
    public void onResume() {
        super.onResume();
        //loadUmur(tab);
        loadMurid(tab);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void showLoading() {
        dialog = DialogBuilder.showLoadingDialog(getContext(), "Updating Data", "Please Wait", false);
    }

    @Override
    public void hideLoading() {
        dialog.dismiss();
    }

    @Override
    public void loadMurid(List<Murid> muridList) {

    }
}
