package com.imaisnaini.daycarereport.ui.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.imaisnaini.daycarereport.R;
import com.imaisnaini.daycarereport.bl.db.model.Ortu;
import com.imaisnaini.daycarereport.bl.network.api.Api;
import com.imaisnaini.daycarereport.bl.network.config.RetrofitBuilder;
import com.imaisnaini.daycarereport.ui.adapter.OrtuAdapter;
import com.imaisnaini.daycarereport.ui.dialog.DialogBuilder;
import com.imaisnaini.daycarereport.ui.presenter.OrtuPresenter;
import com.imaisnaini.daycarereport.ui.view.OrtuView;
import com.jakewharton.threetenabp.AndroidThreeTen;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DetailOrtuActivity extends AppCompatActivity implements OrtuView {

    @BindView(R.id.rvOrtu_detailOrtu)
    RecyclerView rvOrtu;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private List<Ortu> ortuList = new ArrayList<>();
    private OrtuPresenter ortuPresenter;
    private OrtuAdapter ortuAdapter;
    private int id_ortu;
    private Api mApi;
    private Ortu ortu;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_ortu);
        ButterKnife.bind(this);
        AndroidThreeTen.init(this);
        mApi = RetrofitBuilder.builder(this).create(Api.class);
        init();
        //initViews();
    }

    private void init(){
        toolbar.setTitle("Pilih Wali");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        ortuPresenter = new OrtuPresenter(getApplicationContext(), this);
        ortuAdapter =  new OrtuAdapter(getApplicationContext());

        rvOrtu.setHasFixedSize(true);
        rvOrtu.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add_murid, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_next:
                if (!ortuAdapter.getOrtuChecked()){
                    DialogBuilder.showErrorDialog(DetailOrtuActivity.this, "Silahkan pilih wali terlebih dahulu!");
                }else {
                    ortu = ortuAdapter.getSelected();
                    Intent intent = new Intent(DetailOrtuActivity.this, AddMuridActivity.class);
                    intent.putExtra("id_ortu", ortu.getId_ortu());
                    startActivity(intent);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        ortuPresenter.readAll();
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void loadOrtu(List<Ortu> ortuList) {
        ortuAdapter.generate(ortuList);
        ortuAdapter.notifyDataSetChanged();
        rvOrtu.setAdapter(ortuAdapter);

    }
}
