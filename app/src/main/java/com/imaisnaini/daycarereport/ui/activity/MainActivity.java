package com.imaisnaini.daycarereport.ui.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.imaisnaini.daycarereport.R;
import com.imaisnaini.daycarereport.bl.db.dao.GuruDao;
import com.imaisnaini.daycarereport.bl.db.dao.KegiatanDao;
import com.imaisnaini.daycarereport.bl.db.dao.LingkupDao;
import com.imaisnaini.daycarereport.bl.db.dao.MuridDao;
import com.imaisnaini.daycarereport.bl.db.dao.OrtuDao;
import com.imaisnaini.daycarereport.bl.db.dao.ReportDao;
import com.imaisnaini.daycarereport.bl.db.dao.TppaDao;
import com.imaisnaini.daycarereport.bl.db.helper.Db;
import com.imaisnaini.daycarereport.bl.db.model.Guru;
import com.imaisnaini.daycarereport.bl.db.model.Kegiatan;
import com.imaisnaini.daycarereport.bl.db.model.Lingkup;
import com.imaisnaini.daycarereport.bl.db.model.Murid;
import com.imaisnaini.daycarereport.bl.db.model.Ortu;
import com.imaisnaini.daycarereport.bl.db.model.Report;
import com.imaisnaini.daycarereport.bl.db.model.Tppa;
import com.imaisnaini.daycarereport.bl.network.api.Api;
import com.imaisnaini.daycarereport.bl.network.api.SyncWorker;
import com.imaisnaini.daycarereport.bl.network.config.RetrofitBuilder;
import com.imaisnaini.daycarereport.bl.network.model.UserGuru;
import com.imaisnaini.daycarereport.ui.dialog.DialogBuilder;
import com.imaisnaini.daycarereport.ui.fragment.AccountGuruFragment;
import com.imaisnaini.daycarereport.ui.fragment.ListGuruFragment;
import com.imaisnaini.daycarereport.ui.fragment.ListMuridFragment;
import com.imaisnaini.daycarereport.ui.util.PrefUtil;

import java.sql.SQLException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.bottom_navigation_guru)
    BottomNavigationView bottomNav;

    private Api mApi;
    private boolean isFirstRun = true;
    private UserGuru userGuru;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        Db.getInstance().init(this);
        mApi = RetrofitBuilder.builder(this).create(Api.class);
        userGuru = PrefUtil.getGuru(this, PrefUtil.USER_SESSION);
        new DoCloudSync(this).execute();
        initialCheck();
        init();

    }

    // This method to initialaze view
    private void init() {
        Integer mode = getIntent().getIntExtra("mode", 0);

        bottomNav.setOnNavigationItemSelectedListener(navListener);

        if (mode == 0) {
            bottomNav.setSelectedItemId(R.id.nav_childern);
        }else if (mode == 1){
            bottomNav.setSelectedItemId(R.id.nav_guru);
        }else if (mode == 2){
            bottomNav.setSelectedItemId(R.id.nav_akun);
        }
    }

    private BottomNavigationView.OnNavigationItemSelectedListener navListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment selectedFragment = null;

            switch (item.getItemId()){
                case R.id.nav_childern:
                    selectedFragment = new ListMuridFragment();
                    break;
                case R.id.nav_guru:
                    selectedFragment = new ListGuruFragment();
                    break;
                case R.id.nav_akun:
                    selectedFragment = new AccountGuruFragment();
                    break;
            }

            getSupportFragmentManager().beginTransaction().replace(R.id.framelayoutmain,selectedFragment).commit();
            return true;
        }
    };

    private class DoCloudSync extends AsyncTask<Void, Void, Void> {
        private MaterialDialog dialog;
        private final Context ctx;

        private DoCloudSync(Context ctx) {
            this.ctx = ctx;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = DialogBuilder.showLoadingDialog(ctx, "Updating Data", "Please Wait", false);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                SyncWorker.getSyncWorker().syncMurid(ctx, mApi.getMurid(), isFirstRun);
                SyncWorker.getSyncWorker().syncGuru(ctx, mApi.getGuru(), isFirstRun);
                SyncWorker.getSyncWorker().syncOrtu(ctx, mApi.getOrtu(), isFirstRun);
                SyncWorker.getSyncWorker().syncKegiatan(ctx, mApi.getKegiatan(), isFirstRun);
                SyncWorker.getSyncWorker().syncLingkup(ctx, mApi.getLingkup(), isFirstRun);
                SyncWorker.getSyncWorker().syncReport(ctx, mApi.getReport(), isFirstRun);
                //SyncWorker.getSyncWorker().syncTppa(ctx, mApi.getTppa(), isFirstRun);
                if(isFirstRun) Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            dialog.dismiss();
        }
    }

    private void initialCheck(){
        try {
            List<Murid> muridCheck = MuridDao.getMuridDao().read();
            List<Ortu> ortuCheck = OrtuDao.getOrtuDao().read();
            List<Guru> guruCheck = GuruDao.getGuruDao().read();
            List<Kegiatan> kegiatanCheck = KegiatanDao.getKegiatanDao().read();
            List<Report> reportCheck = ReportDao.getReportDao().read();
            List<Lingkup> lingkupCheck = LingkupDao.getLingkupDao().read();
            //List<Tppa> tppaCheck = TppaDao.getTppaDao().read();
            isFirstRun = (muridCheck.isEmpty()|| ortuCheck.isEmpty() || guruCheck.isEmpty() ||
                    kegiatanCheck.isEmpty() || lingkupCheck.isEmpty() || reportCheck.isEmpty() || false);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Log.i("ISFIRSTRUN", String.valueOf(isFirstRun));
    }
}
