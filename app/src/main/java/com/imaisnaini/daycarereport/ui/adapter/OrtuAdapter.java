package com.imaisnaini.daycarereport.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.imaisnaini.daycarereport.R;
import com.imaisnaini.daycarereport.bl.db.model.Ortu;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrtuAdapter extends RecyclerView.Adapter<OrtuAdapter.ViewHolder> {
    Context context;
    List<Ortu> list = new ArrayList<>();
    Ortu selected = new Ortu();
    int mCheckedPostion = -1;
    private boolean isChoosen = false;
    private CompoundButton lastCheckedRB = null;
    //private RadioButton lastCheckedRB = null;


    public OrtuAdapter(Context context) {
        this.context = context;
    }

    public OrtuAdapter(Context context, List<Ortu> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ortu, parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Ortu ortu = list.get(position);
        holder.id = ortu.getId_ortu();
        holder.tvNama.setText(ortu.getNama());
        holder.tvNik.setText(ortu.getNik());
        holder.tvAlamat.setText(ortu.getAlamat());
        //holder.rbOrtu.setChecked(position == mCheckedPostion);
        holder.rbOrtu.setTag(position);
        holder.rbOrtu.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                int tag = (int) compoundButton.getTag();
                if (lastCheckedRB == null) {
                    lastCheckedRB = compoundButton;
                } else if (tag != (int) lastCheckedRB.getTag()) {
                    lastCheckedRB.setChecked(false);
                    lastCheckedRB = compoundButton;
                }
                selected = list.get(position);
                isChoosen = true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.tvNama_itemOrtu) TextView tvNama;
        @BindView(R.id.tvNik_itemOrtu) TextView tvNik;
        @BindView(R.id.tvAlamat_itemOrtu) TextView tvAlamat;
        @BindView(R.id.rb_itemOrtu) RadioButton rbOrtu;
        @BindView(R.id.cv_itemOrtu) CardView cvOrtu;

        private int id;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    public void generate(List<Ortu> ortuList){
        clear();
        list = ortuList;
        notifyDataSetChanged();
    }

    public void clear(){
        list.clear();
        notifyDataSetChanged();
    }

    public boolean getOrtuChecked(){
        return isChoosen;
    }

    public Ortu getSelected(){
        return selected;
    }
}
