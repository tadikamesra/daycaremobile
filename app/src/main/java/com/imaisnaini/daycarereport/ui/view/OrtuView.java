package com.imaisnaini.daycarereport.ui.view;

import com.imaisnaini.daycarereport.bl.db.model.Ortu;

import java.util.List;

public interface OrtuView {
    void showLoading();
    void hideLoading();
    void loadOrtu(List<Ortu> ortuList);
}
