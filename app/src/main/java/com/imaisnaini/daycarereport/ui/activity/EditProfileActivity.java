package com.imaisnaini.daycarereport.ui.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.material.textfield.TextInputEditText;
import com.imaisnaini.daycarereport.R;
import com.imaisnaini.daycarereport.bl.db.model.Guru;
import com.imaisnaini.daycarereport.bl.network.api.Api;
import com.imaisnaini.daycarereport.bl.network.api.SyncWorker;
import com.imaisnaini.daycarereport.bl.network.config.RetrofitBuilder;
import com.imaisnaini.daycarereport.bl.network.model.BaseRespons;
import com.imaisnaini.daycarereport.bl.network.model.UserGuru;
import com.imaisnaini.daycarereport.ui.SetTime;
import com.imaisnaini.daycarereport.ui.dialog.DialogBuilder;
import com.imaisnaini.daycarereport.ui.presenter.GuruPresenter;
import com.imaisnaini.daycarereport.ui.presenter.MuridPresenter;
import com.imaisnaini.daycarereport.ui.util.PrefUtil;
import com.imaisnaini.daycarereport.ui.view.GuruView;
import com.jakewharton.threetenabp.AndroidThreeTen;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.imaisnaini.daycarereport.ui.util.Util.isEempty;

public class EditProfileActivity extends AppCompatActivity implements GuruView {

    private int id_guru;
    private GuruPresenter guruPresenter;
    private Guru guru;
    private UserGuru user;
    private Api mApi;
    private MaterialDialog mDialog;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tedit_nama_editprofile)
    TextInputEditText etNama;
    @BindView(R.id.tedit_nip_editprofile)
    TextInputEditText etNip;
    @BindView(R.id.tedit_email_editprofile)
    TextInputEditText etEmail;
    @BindView(R.id.tedit_jk_editprofile)
    TextInputEditText etJk;
    @BindView(R.id.tedit_notelp_editprofile)
    TextInputEditText etTelp;
//    @BindView(R.id.tedit_pass_editprofile)
//    TextInputEditText etPassword;
    @BindView(R.id.iv_editprofile)
    ImageView ivAvatar;
    private Context ctx;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        ButterKnife.bind(this);
        AndroidThreeTen.init(this);
        initData();
        initViews();
    }

    private void initData(){
        mApi = RetrofitBuilder.builder(this).create(Api.class);
        id_guru = getIntent().getIntExtra("id_guru", 0);
        user = PrefUtil.getGuru(this, PrefUtil.USER_SESSION);
        guruPresenter = new GuruPresenter(getApplicationContext(), this);
        guru = guruPresenter.getByID(user.getIdGuru());
    }

    private void initViews(){
        toolbar.setTitle("Edit Profil");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        etNama.setText(String.valueOf(guru.getNama()));
        etNip.setText(String.valueOf(guru.getNip()));
        etEmail.setText(String.valueOf(guru.getEmail()));
        etJk.setText(String.valueOf(guru.getJk()));
        etTelp.setText(String.valueOf(guru.getTelp()));
        //etPassword.setText(String.valueOf(guru.getPassword()));
        ivAvatar.setImageResource(R.drawable.profile);

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @OnClick(R.id.btn_save_edit) void onUpdate(){
            MaterialDialog dialog = DialogBuilder.showLoadingDialog(EditProfileActivity.this, "Saving Data", "Please Wait", false);
            mApi.updateGuru(user.getIdGuru(), etNama.getText().toString(),
                    etNip.getText().toString(), etEmail.getText().toString(), etJk.getText().toString(),
                    etTelp.getText().toString())
                    .enqueue(new Callback<BaseRespons>() {
                        @Override
                        public void onResponse(Call<BaseRespons> call, Response<BaseRespons> response) {
                            BaseRespons baseRespons = response.body();
                            String message = "";
                            if (baseRespons!=null){
                                if (!baseRespons.getError()){
                                    message = "Data berhasil disimpan.";
                                    SyncWorker.getSyncWorker().syncGuru(ctx, mApi.getGuru(), false);
                                }
                            }
                            if (response.code() == 400){
                                message = "Invalid parameter!";
                            }
                            if (response.code() == 502){
                                message = "Gagal menyimpan ke database.";
                            }
                            Toast.makeText(EditProfileActivity.this, message, Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            onSupportNavigateUp();
                        }

                        @Override
                        public void onFailure(Call<BaseRespons> call, Throwable t) {

                        }
                    });
    }




    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void loadGuru(List<Guru> guruList) {

    }
}
