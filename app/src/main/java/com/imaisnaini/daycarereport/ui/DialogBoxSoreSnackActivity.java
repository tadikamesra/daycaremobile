package com.imaisnaini.daycarereport.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.app.TimePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.TimePicker;

import com.google.android.material.textfield.TextInputEditText;
import com.imaisnaini.daycarereport.R;

import java.util.Calendar;

public class DialogBoxSoreSnackActivity extends AppCompatActivity {

    TextInputEditText etWaktu;
    TimePickerDialog timePickerDialog;
    Calendar calendar;
    int currentHour;
    int currentMinute;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog_box_sore_snack);

        etWaktu = findViewById(R.id.texfil_guru_sore_waktubebas);
        etWaktu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calendar = Calendar.getInstance();
                currentHour = calendar.get(Calendar.HOUR_OF_DAY);
                currentMinute = calendar.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog = new TimePickerDialog(DialogBoxSoreSnackActivity.this,
                        new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker timePicker, int hourOfDay, int minutes) {
                                etWaktu.setText(String.format("%02d:%02d", hourOfDay, minutes));

                            }
                        }, currentHour, currentMinute, true);

                timePickerDialog.show();
            }
        });
    }
}
