package com.imaisnaini.daycarereport.ui.activity;

import android.content.Context;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.material.textfield.TextInputEditText;
import com.imaisnaini.daycarereport.R;
import com.imaisnaini.daycarereport.bl.db.model.Ortu;
import com.imaisnaini.daycarereport.bl.network.api.Api;
import com.imaisnaini.daycarereport.bl.network.api.SyncWorker;
import com.imaisnaini.daycarereport.bl.network.config.RetrofitBuilder;
import com.imaisnaini.daycarereport.bl.network.model.BaseRespons;
import com.imaisnaini.daycarereport.bl.network.model.UserOrtu;
import com.imaisnaini.daycarereport.ui.dialog.DialogBuilder;
import com.imaisnaini.daycarereport.ui.fragment.AccountOrtuFragment;
import com.imaisnaini.daycarereport.ui.presenter.GuruPresenter;
import com.imaisnaini.daycarereport.ui.presenter.OrtuPresenter;
import com.imaisnaini.daycarereport.ui.util.PrefUtil;
import com.imaisnaini.daycarereport.ui.view.OrtuView;
import com.jakewharton.threetenabp.AndroidThreeTen;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfileOrtuActivity extends AppCompatActivity implements OrtuView {

    private int id_ortu;
    private OrtuPresenter ortuPresenter;
    private Ortu ortu;
    private UserOrtu user;
    private Api mApi;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tedit_nama_editprofile_ortu)
    TextInputEditText etNama;
    @BindView(R.id.tedit_nik_editprofile_ortu)
    TextInputEditText etNik;
    @BindView(R.id.tedit_jk_editprofile_ortu)
    TextInputEditText etJk;
    @BindView(R.id.tedit_notelp_editprofile_ortu)
    TextInputEditText etTelp;
    @BindView(R.id.tedit_alamat_editprofile_ortu)
    TextInputEditText etAlamat;
    //    @BindView(R.id.tedit_pass_editprofile)
//    TextInputEditText etPassword;
    @BindView(R.id.iv_editprofileortu)
    ImageView ivAvatar;
    private Context ctx;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile_ortu);
        ButterKnife.bind(this);
        AndroidThreeTen.init(this);
        initData();
        initViews();
    }

    private void initData(){
        mApi = RetrofitBuilder.builder(this).create(Api.class);
        id_ortu = getIntent().getIntExtra("id_ortu", 0);
        user = PrefUtil.getOrtu(this, PrefUtil.USER_SESSION);
        ortuPresenter = new OrtuPresenter(getApplicationContext(), this);
        ortu = ortuPresenter.getByID(user.getIdOrtu());
    }

    private void initViews(){
        toolbar.setTitle("Edit Profil");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        etNama.setText(String.valueOf(ortu.getNama()));
        etNik.setText(String.valueOf(ortu.getNik()));
        etJk.setText(String.valueOf(ortu.getJk()));
        etTelp.setText(String.valueOf(ortu.getTelp()));
        etAlamat.setText(String.valueOf(ortu.getAlamat()));
        //etPassword.setText(String.valueOf(guru.getPassword()));
        ivAvatar.setImageResource(R.drawable.profile);

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @OnClick(R.id.btn_save_edit_ortu) void onUpdate(){
        MaterialDialog dialog = DialogBuilder.showLoadingDialog(EditProfileOrtuActivity.this, "Saving Data", "Please Wait", false);
        mApi.updateOrtu(user.getIdOrtu(), etNama.getText().toString(),
                etNik.getText().toString(), etJk.getText().toString(),
                etTelp.getText().toString(), etAlamat.getText().toString())
                .enqueue(new Callback<BaseRespons>() {
                    @Override
                    public void onResponse(Call<BaseRespons> call, Response<BaseRespons> response) {
                        BaseRespons baseRespons = response.body();
                        String message = "";
                        if (baseRespons!=null){
                            if (!baseRespons.getError()){
                                message = "Data berhasil disimpan.";
                                SyncWorker.getSyncWorker().syncOrtu(ctx, mApi.getOrtu(), false);
                            }
                        }
                        if (response.code() == 400){
                            message = "Invalid parameter!";
                        }
                        if (response.code() == 502){
                            message = "Gagal menyimpan ke database.";
                        }
                        Toast.makeText(EditProfileOrtuActivity.this, message, Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        onSupportNavigateUp();
                    }

                    @Override
                    public void onFailure(Call<BaseRespons> call, Throwable t) {

                    }
                });
    }


    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void loadOrtu(List<Ortu> ortuList) {

    }
}
