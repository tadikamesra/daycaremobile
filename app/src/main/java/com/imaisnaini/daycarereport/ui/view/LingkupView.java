package com.imaisnaini.daycarereport.ui.view;

import com.imaisnaini.daycarereport.bl.db.model.Lingkup;
import com.imaisnaini.daycarereport.bl.network.model.Lingkups;

import java.util.List;

public interface LingkupView {
    void showLoading();
    void hideLoading();
    void loadLingkup(List<Lingkup> lingkups);
}
