package com.imaisnaini.daycarereport.ui;

import android.app.TimePickerDialog;
import android.content.Context;
import android.view.View;
import android.widget.EditText;
import android.widget.TimePicker;

import java.util.Calendar;

public class SetTime implements View.OnFocusChangeListener, TimePickerDialog.OnTimeSetListener {
    private EditText editText;
    private Calendar myCalendar;
    private Context ctx;
    private String timeResult;

    public SetTime(EditText editText, Context ctx){
        this.ctx = ctx;
        this.editText = editText;
        this.editText.setOnFocusChangeListener(this);
        this.myCalendar = Calendar.getInstance();

    }

    public String getTimeResult(){
        return timeResult;
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        // TODO Auto-generated method stub
        if(hasFocus){
            int hour = myCalendar.get(Calendar.HOUR_OF_DAY);
            int minute = myCalendar.get(Calendar.MINUTE);
            new TimePickerDialog(ctx, this, hour, minute, true).show();
        }
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        // TODO Auto-generated method stub
        String hour, min;
        if (hourOfDay < 10){
            hour = "0" + hourOfDay;
        }else {
            hour = String.valueOf(hourOfDay);
        }
        if (minute < 10){
            min = "0" + minute;
        }else {
            min = String.valueOf(minute);
        }
        this.editText.setText(hour + ":" + min);
        timeResult = hour + ":" + min + ":00";
    }

}
