package com.imaisnaini.daycarereport.ui.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.renderscript.RenderScript;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.material.textfield.TextInputEditText;
import com.imaisnaini.daycarereport.R;
import com.imaisnaini.daycarereport.bl.db.model.Guru;
import com.imaisnaini.daycarereport.bl.db.model.Murid;
import com.imaisnaini.daycarereport.bl.db.model.Ortu;
import com.imaisnaini.daycarereport.bl.network.api.Api;
import com.imaisnaini.daycarereport.bl.network.api.SyncWorker;
import com.imaisnaini.daycarereport.bl.network.config.RetrofitBuilder;
import com.imaisnaini.daycarereport.bl.network.model.BaseRespons;
import com.imaisnaini.daycarereport.ui.dialog.DialogBuilder;
import com.imaisnaini.daycarereport.ui.fragment.ListMuridFragment;
import com.imaisnaini.daycarereport.ui.presenter.GuruPresenter;
import com.imaisnaini.daycarereport.ui.presenter.MuridPresenter;
import com.imaisnaini.daycarereport.ui.presenter.OrtuPresenter;
import com.imaisnaini.daycarereport.ui.util.PrefUtil;
import com.imaisnaini.daycarereport.ui.view.MuridView;
import com.imaisnaini.daycarereport.ui.view.OrtuView;
import com.jakewharton.threetenabp.AndroidThreeTen;

import org.json.JSONObject;

import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddMuridActivity extends AppCompatActivity implements OrtuView {

    Calendar calendar;
    int year;
    int month;
    int date;
    private String selected_jk;
    private int id_murid, id_ortu;
    private MuridPresenter muridPresenter;
    private OrtuPresenter ortuPresenter;
    private ArrayAdapter<CharSequence> adapter;
    private Ortu ortu;
    private Murid murid;
    //private UserGuru user;
    private Api mApi;
    private MaterialDialog mDialog;
    private Context ctx;


    @BindView(R.id.texfil_addmurid_nama)
    TextInputEditText etNama;
    @BindView(R.id.texfil_addmurid_nik)
    TextInputEditText etNik;
    @BindView(R.id.texfil_addmurid_tgl)
    TextInputEditText etTgl;
//    @BindView(R.id.texfil_addmurid_jk)
//    TextInputEditText etJk;
    @BindView(R.id.sp_daftarJk_addMurid)
    Spinner spJk;
    @BindView(R.id.texfil_addmurid_bb)
    TextInputEditText etBb;
    @BindView(R.id.texfil_addmurid_tb)
    TextInputEditText etTb;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
//        @BindView(R.id.btn_cancel_addguru)
//        Button btn_cancel;
//        @BindView(R.id.btn_ok_addguru)
//        Button btn_ok;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_murid);
        ButterKnife.bind(this);
        AndroidThreeTen.init(this);


        initData();
        initViews();

    }

    private void initData(){
        mApi = RetrofitBuilder.builder(this).create(Api.class);
        id_ortu = getIntent().getIntExtra("id_ortu", 0);
        //user = PrefUtil.getGuru(this, PrefUtil.USER_SESSION);
        ortuPresenter = new OrtuPresenter(getApplicationContext(), this);
        ortu = ortuPresenter.getByID(id_ortu);
    }


    private void initViews(){
        toolbar.setTitle("Tambah Anak");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @OnItemSelected(R.id.sp_daftarJk_addMurid) void jkSelected(int position){
        selected_jk = (String) spJk.getAdapter().getItem(position);
    }

    @OnItemSelected(value = R.id.sp_daftarJk_addMurid, callback = OnItemSelected.Callback.NOTHING_SELECTED)
    void onNothingSelected() {

    }

    @OnClick(R.id.texfil_addmurid_tgl) void showTimeDialog(){
        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        date = calendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(AddMuridActivity.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        etTgl.setText(String.format("%02d/%02d/%02d", year, month+1, dayOfMonth));
                    }

                }, year, month, date);

        datePickerDialog.show();
    }

    @OnClick(R.id.btn_ok_addmurid) void onAdd(){
        MaterialDialog dialog = DialogBuilder.showLoadingDialog(AddMuridActivity.this, "Add Data", "Please Wait", false);
        mApi.addMurid(etNama.getText().toString(),
                etNik.getText().toString(),etTgl.getText().toString(),
                selected_jk,etBb.getText().toString(), etTb.getText().toString(), id_ortu)
                .enqueue(new Callback<BaseRespons>() {
                    @Override
                    public void onResponse(Call<BaseRespons> call, Response<BaseRespons> response) {
                        BaseRespons baseRespons = response.body();
                        String message = "";
                        if (baseRespons!=null){
                            if(!baseRespons.getError()){
                                message = "Data berhasil ditambahkan";
                                SyncWorker.getSyncWorker().syncMurid(getApplicationContext(), mApi.getMurid(), false);
                            }
                        }
                        if (response.code() == 400){
                            message = "Invalid parameter!";
                        }
                        if (response.code() == 502){
                            message = "Gagal menyimpan ke database.";
                        }
                        Toast.makeText(AddMuridActivity.this, message, Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                        Intent intent = new Intent(AddMuridActivity.this, MainActivity.class);
                        startActivity(intent);
//                        try {
//                            Thread.sleep(1000);
//                        } catch (InterruptedException e) {
//                            e.printStackTrace();
//                        }
//                        onSupportNavigateUp();
                    }

                    @Override
                    public void onFailure(Call<BaseRespons> call, Throwable t) {

                    }
                });
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void loadOrtu(List<Ortu> ortuList) {

    }

}
