package com.imaisnaini.daycarereport.ui.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.imaisnaini.daycarereport.R;
import com.imaisnaini.daycarereport.ui.fragment.LoginGuruFragment;
import com.imaisnaini.daycarereport.ui.fragment.LoginOrtuFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity {
    @BindView(R.id.login_container)
    FrameLayout frameLayout;
    @BindView(R.id.login_tvCaption)
    TextView tvCaption;
    @BindView(R.id.login_bottomNav)
    BottomNavigationView bottomNav;

    private boolean mode = true;
    private Fragment fragmentGuru = new LoginGuruFragment();
    private Fragment fragmentOrtu = new LoginOrtuFragment();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        switchFragment();
    }

    @OnClick(R.id.login_switch) void switchFragment(){
        if (mode){
            getSupportFragmentManager().beginTransaction().replace(R.id.login_container, fragmentGuru).commit();
            mode = false;
            tvCaption.setText("Orang Tua");
            tvCaption.setTextColor(getResources().getColor(R.color.textSecondary));
            bottomNav.setBackgroundColor(getResources().getColor(R.color.primaryLight));
        }else {
            getSupportFragmentManager().beginTransaction().replace(R.id.login_container, fragmentOrtu).commit();
            mode = true;
            tvCaption.setText("Guru");
            tvCaption.setTextColor(getResources().getColor(R.color.textAccent));
            bottomNav.setBackgroundColor(getResources().getColor(R.color.secondary));
        }
    }
}
