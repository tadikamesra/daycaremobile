package com.imaisnaini.daycarereport.ui.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.TimePickerDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.imaisnaini.daycarereport.R;
import com.imaisnaini.daycarereport.bl.db.dao.TppaDao;
import com.imaisnaini.daycarereport.bl.db.model.Lingkup;
import com.imaisnaini.daycarereport.bl.db.model.Murid;
import com.imaisnaini.daycarereport.bl.db.model.Report;
import com.imaisnaini.daycarereport.bl.db.model.Tppa;
import com.imaisnaini.daycarereport.bl.network.api.Api;
import com.imaisnaini.daycarereport.bl.network.config.RetrofitBuilder;
import com.imaisnaini.daycarereport.bl.network.model.BaseRespons;
import com.imaisnaini.daycarereport.bl.network.model.Tppas;
import com.imaisnaini.daycarereport.bl.network.model.UserGuru;
import com.imaisnaini.daycarereport.ui.adapter.LingkupAdapter;
import com.imaisnaini.daycarereport.ui.adapter.TppaAdapter;
import com.imaisnaini.daycarereport.ui.dialog.DialogBuilder;
import com.imaisnaini.daycarereport.ui.presenter.LingkupPresenter;
import com.imaisnaini.daycarereport.ui.presenter.ReportPresenter;
import com.imaisnaini.daycarereport.ui.presenter.TppaPresenter;
import com.imaisnaini.daycarereport.ui.util.PrefUtil;
import com.imaisnaini.daycarereport.ui.view.LingkupView;
import com.imaisnaini.daycarereport.ui.view.ReportView;
import com.imaisnaini.daycarereport.ui.view.TppaView;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class KegiatanPagiActivity extends AppCompatActivity  implements LingkupView, TppaView, ReportView {
    @BindView(R.id.spLingkup_kegiatan) Spinner spLingkup;
    @BindView(R.id.spLingkupChild_kegiatan) Spinner spLingkupChild;
    @BindView(R.id.layoutChild_kegiatan) LinearLayout layoutChild;
    @BindView(R.id.layoutTppa_kegiatan) LinearLayout layoutTppa;
    @BindView(R.id.rvTppa_kegiatan) RecyclerView rvTppa;
    @BindView(R.id.etWaktu_kegiatan) EditText etWaktu;
    @BindView(R.id.toolbar) Toolbar toolbar;

    Calendar calendar;
    int currentHour;
    int currentMinute;
    int idKegiatan, idMurid, usia;
    private Api mApi;
    private MaterialDialog materialDialog;
    private Lingkup mParent, mChild;
    private List<Lingkup> mParentList, mChildList = new ArrayList<>();
    private List<Tppa> mTppaList = new ArrayList<>();
    private LingkupPresenter mLingkupPresenter;
    private TppaPresenter mTppaPresenter;
    private ReportPresenter mReportPresenter;
    private LingkupAdapter mParentAdapter, mChildAdapter;
    private TppaAdapter mTppaAdapter;
    private UserGuru user;
    private Report activeReport;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kegiatan_pagi);
        ButterKnife.bind(this);
        mApi = RetrofitBuilder.builder(this).create(Api.class);
        user = PrefUtil.getGuru(this, PrefUtil.USER_SESSION);
        initData();
        initViews();
    }

    private void initData(){
        idKegiatan = getIntent().getIntExtra("idKegiatan", 0);
        idMurid = getIntent().getIntExtra("idMurid", 0);
        usia = getIntent().getIntExtra("usia", 0);

        mLingkupPresenter = new LingkupPresenter(getApplicationContext(), this);
        mTppaPresenter = new TppaPresenter(getApplicationContext(), mApi, this);
        mReportPresenter = new ReportPresenter(this);

        mParentList = mLingkupPresenter.getParent();
        mParentAdapter = new LingkupAdapter(getApplicationContext(), mParentList);
        activeReport = mReportPresenter.getActiveReport(idMurid, idKegiatan);
    }

    private void initViews(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Kegiatan Pagi");

        spLingkup.setAdapter(mParentAdapter);
        rvTppa.setHasFixedSize(true);
        rvTppa.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
    }

    private boolean initTppa(int idLingkup){
        mTppaList = mTppaPresenter.getByLingkupUsia(idLingkup, usia);
        if (mTppaList.isEmpty()){
            mApi.getTPPA(idLingkup, usia).enqueue(new Callback<Tppas>() {
                @Override
                public void onResponse(Call<Tppas> call, Response<Tppas> response) {
                    Log.i("TPPA_GET", response.message());
                    Tppas list = response.body();
                    for (Tppas.Tppa tppa : list.getTppa()) {
                        Tppa obj = new Tppa();
                        obj.setId_tppa(tppa.getIdTppa());
                        obj.setId_lingkup(tppa.getIdLingkup());
                        obj.setUsia(tppa.getUsia());
                        obj.setInterval(tppa.getInterval());
                        obj.setKeterangan(tppa.getKeterangan());

                        try {
                            TppaDao.getTppaDao().add(obj);
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<Tppas> call, Throwable t) {
                    DialogBuilder.showErrorDialog(KegiatanPagiActivity.this, t.getMessage());
                    Log.i("TPPA_GET", t.getMessage());
                }
            });
            return false;
        }
        return true;
    }

    @OnItemSelected(R.id.spLingkup_kegiatan) void parentSelected(int position){
        mParent = mLingkupPresenter.getLingkup(mParentList.get(position).getId_lingkup());
        mChildList = mLingkupPresenter.getChild(mParent.getId_lingkup(), usia);
        if (!mChildList.isEmpty()) {
            mChildAdapter = new LingkupAdapter(KegiatanPagiActivity.this, mChildList);
            layoutChild.setVisibility(View.VISIBLE);
            layoutTppa.setVisibility(View.GONE);
            spLingkupChild.setAdapter(mChildAdapter);
        }else {
            layoutChild.setVisibility(View.GONE);
            if (!initTppa(mParent.getId_lingkup())) {
                mTppaList = mTppaPresenter.getByLingkupUsia(mParent.getId_lingkup(), usia);
            }
            mTppaAdapter = new TppaAdapter(KegiatanPagiActivity.this);
            mTppaAdapter.generate(mTppaList);
            rvTppa.setAdapter(mTppaAdapter);
            layoutTppa.setVisibility(View.VISIBLE);
        }
    }

    @OnItemSelected(value = {R.id.spLingkup_kegiatan, R.id.spLingkupChild_kegiatan}, callback = OnItemSelected.Callback.NOTHING_SELECTED)
    void  onNothingSelected(){

    }

    @OnItemSelected(R.id.spLingkupChild_kegiatan) void childSelected(int position){
        mChild = mChildList.get(position);
        if (!initTppa(mChild.getId_lingkup())) {
            mTppaList = mTppaPresenter.getByLingkupUsia(mChild.getId_lingkup(), usia);
        }
        // TODO : list tppa belum ke load saat initial awal
        mTppaAdapter = new TppaAdapter(KegiatanPagiActivity.this);
        mTppaAdapter.generate(mTppaList);
        rvTppa.setAdapter(mTppaAdapter);
        layoutTppa.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.etWaktu_kegiatan) void showTimeDialog(){
        calendar = Calendar.getInstance();
        currentHour = calendar.get(Calendar.HOUR_OF_DAY);
        currentMinute = calendar.get(Calendar.MINUTE);
        TimePickerDialog timePickerDialog = new TimePickerDialog(KegiatanPagiActivity.this,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hourOfDay, int minutes) {
                        etWaktu.setText(String.format("%02d:%02d", hourOfDay, minutes));

                    }
                }, currentHour, currentMinute, true);

        timePickerDialog.show();
    }

    @OnClick(R.id.btn_ok_kppagi) void onSave(){
        if (!mTppaAdapter.getTppaChecked().isEmpty()){
            materialDialog = DialogBuilder.showLoadingDialog(KegiatanPagiActivity.this, "Saving Data", "Please wait..", false);
            materialDialog.show();
            mApi.addReport(idMurid,
                    user.getIdGuru(),
                    idKegiatan,
                    ".",
                    etWaktu.getText().toString()+":00",
                    mTppaAdapter.getTppaChecked().get(0).getId_tppa())
                    .enqueue(new Callback<BaseRespons>() {
                        @Override
                        public void onResponse(Call<BaseRespons> call, Response<BaseRespons> response) {
                            BaseRespons baseRespons = response.body();
                            String msg = "";
                            if (baseRespons!=null){
                                if (!baseRespons.getError()){
                                    msg = "Data berhasil disimpan.";
                                }
                            }
                            if (response.code() == 400){
                                msg = "Invalid parameter!";
                            }
                            if (response.code() == 502){
                                msg = "Gagal menyimpan ke database.";
                            }
                            Toast.makeText(KegiatanPagiActivity.this, msg, Toast.LENGTH_SHORT).show();
                            materialDialog.dismiss();
                            onSupportNavigateUp();
                        }

                        @Override
                        public void onFailure(Call<BaseRespons> call, Throwable t) {

                        }
                    });
        }else {
            DialogBuilder.showErrorDialog(KegiatanPagiActivity.this, "Anda belum memilih TPPA");
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void showLoading() {
        materialDialog = DialogBuilder.showLoadingDialog(KegiatanPagiActivity.this, "Syncroning Data", "Please Wait", false);
    }

    @Override
    public void hideLoading() {
        materialDialog.dismiss();
    }

    @Override
    public void loadReport(List<Report> reportList) {

    }

    @Override
    public void loadTppa(List<Tppa> tppaList) {
    }

    @Override
    public void loadLingkup(List<Lingkup> lingkups) {
    }
}
