package com.imaisnaini.daycarereport.ui.fragment;


import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.material.textfield.TextInputEditText;
import com.imaisnaini.daycarereport.R;
import com.imaisnaini.daycarereport.bl.network.api.Api;
import com.imaisnaini.daycarereport.bl.network.config.RetrofitBuilder;
import com.imaisnaini.daycarereport.bl.network.model.UserOrtu;
import com.imaisnaini.daycarereport.ui.activity.HomeActivity;
import com.imaisnaini.daycarereport.ui.activity.MainActivity;
import com.imaisnaini.daycarereport.ui.dialog.DialogBuilder;
import com.imaisnaini.daycarereport.ui.util.NikValidator;
import com.imaisnaini.daycarereport.ui.util.PrefUtil;
import com.imaisnaini.daycarereport.ui.util.TeleponValidator;
//import com.imaisnaini.daycarereport.ui.util.NikValidator;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.imaisnaini.daycarereport.ui.util.Util.isEempty;


/**
 * A simple {@link Fragment} subclass.
 */
public class LoginOrtuFragment extends Fragment {

    @BindView(R.id.login_ortu_tietNik)
    TextInputEditText etNik;
    @BindView(R.id.login_ortu_tietTelepon)
    TextInputEditText etTelepon;

    NikValidator nikValidator;
    TeleponValidator teleponValidator;

    private String nik;
    private String telepon;
    private Api mApi;

    public LoginOrtuFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // if user loged in move to main activity
        if(isSessionLogin()){
            startActivity(new Intent(getActivity(), HomeActivity.class));
            getActivity().finish();
        }
        // Inflate the layout for this fragment
//        View viewFragHome =  inflater.inflate(R.layout.fragment_login_ortu, container, false);
//        ButterKnife.bind(this, viewFragHome);
//        mApi = RetrofitBuilder.builder(getActivity()).create((Api.class));
//        return viewFragHome;
        View view = inflater.inflate(R.layout.fragment_login_ortu, container, false);
        ButterKnife.bind(this, view);
        mApi = RetrofitBuilder.builder(getActivity()).create(Api.class);
        return view;
    }

    boolean isNik(EditText text){
        nikValidator = new NikValidator();
        String nik = text.getText().toString();
        return nikValidator.isValid(nik);
    }

    boolean isTelepon(EditText text){
        teleponValidator = new TeleponValidator();
        String telepon = text.getText().toString();
        return  teleponValidator.isValid(telepon);
    }

    @OnClick(R.id.login_ortu_btnLogin) void onLogin(){
        if(isEempty(etNik)){
            etNik.setError("Nik harus diisi!");
        } else if(isEempty(etTelepon)){
            etTelepon.setError("Telepon harus diisi");
        } else if (!isNik(etNik)){
            etNik.setError("Nik tidak valid");
        } else if (!isTelepon(etTelepon)){
            etTelepon.setError("Telepon tidak valid");
        } else {
            loginAct();
        }
//        startActivity(new Intent(getContext(), HomeActivity.class));
//        getActivity().finish();
    }

    void loginAct(){
        nik = etNik.getText().toString();
        telepon = etTelepon.getText().toString();
        final MaterialDialog dialog = DialogBuilder.showLoadingDialog(getContext(), "Updating Data", "Please wait..", false);
        mApi.loginOrtu(nik, telepon).enqueue(new Callback<UserOrtu>() {
            @Override
            public void onResponse(Call<UserOrtu> call, Response<UserOrtu> response) {
                UserOrtu user = response.body();
                Log.i("USER_LOGIN", response.message());

                if(user != null){
                    if (!user.getError()){
                        PrefUtil.putOrtu(getActivity(), PrefUtil.USER_SESSION, user);
                        Intent intent = new Intent(getActivity(), HomeActivity.class);
                        startActivity(intent);
                        getActivity().finish();
                    }
                    Toast.makeText(getActivity(), user.getMessage(), Toast.LENGTH_SHORT).show();
                }
                if (response.code() == 403){
                    etTelepon.requestFocus();
                    etTelepon.setError(getString(R.string.error_telepon));
                }
                if (response.code() == 404){
                    etNik.requestFocus();
                    etNik.setError(getString(R.string.error_nik));
                }
                dialog.dismiss();
            }

            @Override
            public void onFailure(Call<UserOrtu> call, Throwable t) {
                dialog.dismiss();
                Log.i("USER_LOGIN",t.getMessage());
                DialogBuilder.showErrorDialog(getActivity(), "Gagal Login");
            }
        });
    }
    boolean isSessionLogin(){
        return PrefUtil.getOrtu(getActivity(),PrefUtil.USER_SESSION) != null;
    }

//    private class NikValidator {
//    }
//
//    private class TeleponValidator {
//    }
}

