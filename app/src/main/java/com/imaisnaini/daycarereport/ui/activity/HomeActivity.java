package com.imaisnaini.daycarereport.ui.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.imaisnaini.daycarereport.R;
import com.imaisnaini.daycarereport.ui.fragment.AccountOrtuFragment;
import com.imaisnaini.daycarereport.ui.fragment.ListKegiatanOrtuFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends AppCompatActivity {
    @BindView(R.id.bottom_navigation_ortu)
    BottomNavigationView bottomNav;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        init();
    }
    // This method to initialaze view
    private void init() {
        Integer mode = getIntent().getIntExtra("mode", 0);

        bottomNav.setOnNavigationItemSelectedListener(navListener);

//        if (mode == 0) {
//            bottomNav.setSelectedItemId(R.id.navigation_home);
//        }else
        if (mode == 0){
            bottomNav.setSelectedItemId(R.id.navigation_list);
        }else if (mode == 1){
            bottomNav.setSelectedItemId(R.id.navigation_account);
        }
    }

    private BottomNavigationView.OnNavigationItemSelectedListener navListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment selectedFragment = null;

            switch (item.getItemId()){
//                case R.id.navigation_home:
//                    selectedFragment = new HomeOrtuFragment();
//                    break;
                case R.id.navigation_list:
                    selectedFragment = new ListKegiatanOrtuFragment();
                    break;
                case R.id.navigation_account:
                    selectedFragment = new AccountOrtuFragment();
                    break;
            }

            getSupportFragmentManager().beginTransaction().replace(R.id.framelayouthome,selectedFragment).commit();
            return true;
        }
    };
}
