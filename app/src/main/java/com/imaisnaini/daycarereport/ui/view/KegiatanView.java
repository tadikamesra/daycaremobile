package com.imaisnaini.daycarereport.ui.view;

import com.imaisnaini.daycarereport.bl.db.model.Kegiatan;

import java.util.List;

public interface KegiatanView {
    void showLoading();
    void hideLoading();
    void loadKegiatan(List<Kegiatan> kegiatanList);
}
