package com.imaisnaini.daycarereport.ui.util;

import android.util.Patterns;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TeleponValidator {
    public boolean isValid(String teleponStr){
        Pattern pattern = Patterns.PHONE;
        Matcher matcher = pattern.matcher(teleponStr);
        return matcher.matches();
    }
}
