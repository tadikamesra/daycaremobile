package com.imaisnaini.daycarereport.ui.util;

import android.util.Patterns;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NikValidator {
    final String NIK_PATTERN ="[0-9]{16}";
    Pattern pattern;
    Matcher matcher;

    private  String str;
    public boolean isValid(String nik){
        pattern = Pattern.compile(NIK_PATTERN);
        matcher = pattern.matcher(nik);
        if (nik.length() != 16){
            setString("NIK harus 16 digit");
            return false;
        }
        if (!matcher.matches()) {
            setString("NIK harus berbentuk angka");
            return false;
        }
        return true;
    }
    public void setString(String str){
        this.str = str;
    }
    public String getString(){
        return str;
    }
}

