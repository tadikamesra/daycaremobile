package com.imaisnaini.daycarereport.ui.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import com.imaisnaini.daycarereport.ui.DialogBoxSoreMandiActivity;
import com.imaisnaini.daycarereport.ui.DialogBoxSoreMengajiActivity;
import com.imaisnaini.daycarereport.ui.DialogBoxSorePulangActivity;
import com.imaisnaini.daycarereport.ui.DialogBoxSoreSnackActivity;
import com.imaisnaini.daycarereport.R;

public class KegiatanSoreActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kegiatan_sore);

        ImageButton imgbtnback = (ImageButton) findViewById(R.id.ib_arrowbacksore);
        ImageButton imgbtnmandi = (ImageButton) findViewById(R.id.ib_arrowmandisore);
        ImageButton imgbtnsnack = (ImageButton) findViewById(R.id.ib_arrowsnacksore);
        ImageButton imgbtnmengaji = (ImageButton) findViewById(R.id.ib_arrowmengajisore);
        ImageButton imgbtnpulang = (ImageButton) findViewById(R.id.ib_arrowpulangsore);

        //btn
        imgbtnback.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v){
                Intent i = new Intent(KegiatanSoreActivity.this, ListKegiatanGuruActivity.class);
                startActivity(i);
            }
        });

        imgbtnmandi.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v){
                Intent i = new Intent(KegiatanSoreActivity.this, DialogBoxSoreMandiActivity.class);
                startActivity(i);
            }
        });

        imgbtnsnack.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v){
                Intent i = new Intent(KegiatanSoreActivity.this, DialogBoxSoreSnackActivity.class);
                startActivity(i);
            }
        });

        imgbtnmengaji.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v){
                Intent i = new Intent(KegiatanSoreActivity.this, DialogBoxSoreMengajiActivity.class);
                startActivity(i);
            }
        });


        imgbtnpulang.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v){
                Intent i = new Intent(KegiatanSoreActivity.this, DialogBoxSorePulangActivity.class);
                startActivity(i);
            }
        });




    }
}
