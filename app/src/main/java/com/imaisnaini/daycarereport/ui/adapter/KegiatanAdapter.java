package com.imaisnaini.daycarereport.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.material.textfield.TextInputEditText;
import com.imaisnaini.daycarereport.R;
import com.imaisnaini.daycarereport.bl.db.model.Kegiatan;
import com.imaisnaini.daycarereport.bl.db.model.Report;
import com.imaisnaini.daycarereport.bl.network.api.Api;
import com.imaisnaini.daycarereport.bl.network.api.SyncWorker;
import com.imaisnaini.daycarereport.bl.network.config.RetrofitBuilder;
import com.imaisnaini.daycarereport.bl.network.model.BaseRespons;
import com.imaisnaini.daycarereport.ui.SetTime;
import com.imaisnaini.daycarereport.ui.activity.KegiatanSiangActivity;
import com.imaisnaini.daycarereport.ui.activity.ListKegiatanGuruActivity;
import com.imaisnaini.daycarereport.ui.dialog.DialogBuilder;
import com.imaisnaini.daycarereport.ui.presenter.ReportPresenter;
import com.imaisnaini.daycarereport.ui.util.PrefUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class KegiatanAdapter extends RecyclerView.Adapter<KegiatanAdapter.ViewHolder> {
    Context context;
    Activity activity;
    MaterialDialog materialDialog;
    List<Kegiatan> kegiatanList = new ArrayList<>();
    List<Report> reportList = new ArrayList<>();
    private Api mApi;
    private ReportPresenter mReportPresenter;
    private Report activeReport;

    public KegiatanAdapter(Context context) {
        this.context = context;
    }

    public KegiatanAdapter(Context context, List<Kegiatan> kegiatanList) {
        this.context = context;
        this.kegiatanList = kegiatanList;
    }

    public KegiatanAdapter(Activity activity, List<Kegiatan> kegiatanList) {
        this.activity = activity;
        this.kegiatanList = kegiatanList;
        mApi = RetrofitBuilder.builder(activity).create(Api.class);
        mReportPresenter = new ReportPresenter();
        for (int i = 0; i < kegiatanList.size(); i++){
            reportList.add(mReportPresenter.getActiveReport(KegiatanSiangActivity.idMurid, kegiatanList.get(i).getId_kegiatan()));
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_kegiatan, parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvTitle.setText(kegiatanList.get(position).getKegetian());
        if (reportList.get(position)==null){
            holder.tvKetrangan.setVisibility(View.GONE);
            holder.tvWaktu.setVisibility(View.GONE);
        }else {
            holder.tvKetrangan.setVisibility(View.VISIBLE);
            holder.tvWaktu.setVisibility(View.VISIBLE);
            holder.tvKetrangan.setText(reportList.get(position).getKeterangan());
            holder.tvWaktu.setText(reportList.get(position).getWaktu().substring(0,5));
        }

        holder.layoutKegiatan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                materialDialog = new MaterialDialog.Builder(activity)
                        .customView(R.layout.activity_dialog_box_kegiatan, true)
                        .positiveText(R.string.simpan)
                        .negativeText(R.string.batal)
                        .build();
                TextView tvTitle = materialDialog.getCustomView().findViewById(R.id.tvTitle_dialogKegiatan);
                TextInputEditText etKeterangan = materialDialog.getCustomView().findViewById(R.id.tietKeterangan_dialogKegiatan);
                TextInputEditText etWaktu = materialDialog.getCustomView().findViewById(R.id.tietWaktu_dialogKegiatan);
                SetTime fromTime = new SetTime(etWaktu, activity);
                tvTitle.setText(kegiatanList.get(position).getKegetian());

                if (reportList.get(position)!=null){
                    etKeterangan.setText(reportList.get(position).getKeterangan());
                    etWaktu.setText(reportList.get(position).getWaktu().substring(0,5));
                }
                materialDialog.getActionButton(DialogAction.POSITIVE).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view1) {
                        MaterialDialog dialog = DialogBuilder.showLoadingDialog(activity, "Saving Data", "Please Wait", false);
                        if (reportList.get(position)==null) {
                            mApi.addReport(KegiatanSiangActivity.idMurid,
                                    PrefUtil.getGuru(activity, PrefUtil.USER_SESSION).getIdGuru(),
                                    kegiatanList.get(position).getId_kegiatan(),
                                    etKeterangan.getText().toString(),
                                    fromTime.getTimeResult())
                                    .enqueue(new Callback<BaseRespons>() {
                                        @Override
                                        public void onResponse(Call<BaseRespons> call, Response<BaseRespons> response) {
                                            BaseRespons baseRespons = response.body();
                                            String msg = "";
                                            if (baseRespons != null) {
                                                if (!baseRespons.getError()) {
                                                    msg = "Data berhasil disimpan.";
                                                }
                                            }
                                            if (response.code() == 400) {
                                                msg = "Invalid parameter!";
                                            }
                                            if (response.code() == 502) {
                                                msg = "Gagal menyimpan ke database.";
                                            }
                                            Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();
                                            dialog.dismiss();
                                        }

                                        @Override
                                        public void onFailure(Call<BaseRespons> call, Throwable t) {

                                        }
                                    });
                        }else {
                            mApi.updateReport(reportList.get(position).getId_report(),
                                    KegiatanSiangActivity.idMurid,
                                    PrefUtil.getGuru(activity, PrefUtil.USER_SESSION).getIdGuru(),
                                    kegiatanList.get(position).getId_kegiatan(),
                                    etKeterangan.getText().toString(),
                                    etWaktu.getText().toString()+":00")
                                    .enqueue(new Callback<BaseRespons>() {
                                        @Override
                                        public void onResponse(Call<BaseRespons> call, Response<BaseRespons> response) {
                                            BaseRespons baseRespons = response.body();
                                            String msg = "";
                                            if (baseRespons != null) {
                                                if (!baseRespons.getError()) {
                                                    msg = "Data berhasil diupdate.";
                                                }
                                            }
                                            if (response.code() == 400) {
                                                msg = "Invalid parameter!";
                                            }
                                            if (response.code() == 502) {
                                                msg = "Gagal menyimpan ke database.";
                                            }
                                            Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();
                                            dialog.dismiss();
                                        }

                                        @Override
                                        public void onFailure(Call<BaseRespons> call, Throwable t) {

                                        }
                                    });
                        }
                        materialDialog.dismiss();
                    }
                });
                materialDialog.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return kegiatanList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.tvKeterangan_itemKegiatan)
        TextView tvKetrangan;
        @BindView(R.id.tvTitle_itemKegiatan)
        TextView tvTitle;
        @BindView(R.id.tvWaktu_itemKegiatan)
        TextView tvWaktu;
        @BindView(R.id.layout_itemKegiatan)
        RelativeLayout layoutKegiatan;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    class RecyclerTouchListener implements RecyclerView.OnItemTouchListener{

        @Override
        public boolean onInterceptTouchEvent(@NonNull RecyclerView rv, @NonNull MotionEvent e) {
            return false;
        }

        @Override
        public void onTouchEvent(@NonNull RecyclerView rv, @NonNull MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }

    public void generate(List<Kegiatan> list){
        clear();
        this.kegiatanList = list;
        notifyDataSetChanged();
    }

    public void generateReport(List<Report> list){
        reportList.clear();
        notifyDataSetChanged();
        this.reportList = list;
        notifyDataSetChanged();
    }

    public void clear(){
        kegiatanList.clear();
        notifyDataSetChanged();
    }
}
