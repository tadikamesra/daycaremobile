package com.imaisnaini.daycarereport.ui.presenter;


import android.content.Context;

import com.imaisnaini.daycarereport.bl.db.dao.MuridDao;
import com.imaisnaini.daycarereport.bl.db.model.Murid;
import com.imaisnaini.daycarereport.ui.view.MuridView;
import com.jakewharton.threetenabp.AndroidThreeTen;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MuridPresenter {
    private MuridView muridView;

    public MuridPresenter(Context context, MuridView muridView) {
        this.muridView = muridView;
        AndroidThreeTen.init(context);
    }

    public void load(){
        muridView.showLoading();
        List<Murid> list = new ArrayList<>();
        try {
            list = MuridDao.getMuridDao().read();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        muridView.loadMurid(list);
        muridView.hideLoading();
    }

    public List<Murid> loadByAge(int age, int interval){
        muridView.showLoading();
        List<Murid> list = new ArrayList<>();
        List<Murid> selected = new ArrayList<>();
        try {
            list = MuridDao.getMuridDao().read();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        for (Murid murid : list){
            if (age <= murid.getUsia() && murid.getUsia() <= interval){
                selected.add(murid);
            }
        }
        //muridView.loadTppa(selected);
        muridView.hideLoading();
        return selected;
    }

    public List<Murid> getByOrtu(int ortu){
        List<Murid> list = new ArrayList<>();
        try {
            list = MuridDao.getMuridDao().getByOrtu(ortu);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public Murid getByID(int id){
        Murid murid = null;
        try {
            murid = MuridDao.getMuridDao().getByID(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return murid;
    }
}
