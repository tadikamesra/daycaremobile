package com.imaisnaini.daycarereport.ui.presenter;

import android.content.Context;
import android.util.Log;

import com.imaisnaini.daycarereport.bl.db.dao.TppaDao;
import com.imaisnaini.daycarereport.bl.db.model.Tppa;
import com.imaisnaini.daycarereport.bl.network.api.Api;
import com.imaisnaini.daycarereport.bl.network.api.SyncWorker;
import com.imaisnaini.daycarereport.bl.network.model.Tppas;
import com.imaisnaini.daycarereport.ui.dialog.DialogBuilder;
import com.imaisnaini.daycarereport.ui.view.TppaView;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TppaPresenter {
    Context context;
    private Api mApi;
    private TppaView tppaView;

    public TppaPresenter(Context context, TppaView tppaView) {
        this.context = context;
        this.tppaView = tppaView;
    }

    public TppaPresenter(Context context, Api mApi, TppaView tppaView) {
        this.context = context;
        this.mApi = mApi;
        this.tppaView = tppaView;
    }

    public List<Tppa> getByLingkup(int idLingkup){
        List<Tppa> list = new ArrayList<>();
        tppaView.showLoading();
        try {
            list = TppaDao.getTppaDao().getByLingkup(idLingkup);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        tppaView.hideLoading();
        return list;
    }

    public List<Tppa> getByLingkupUsia(int idLingkup, int usia){
        List<Tppa> list = new ArrayList<>();
        List<Tppa> listUsia = new ArrayList<>();
        tppaView.showLoading();
        try {
            list = TppaDao.getTppaDao().getByLingkup(idLingkup);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        for (Tppa tppa : list){
            if (tppa.getUsia() <= usia && (tppa.getUsia()+tppa.getInterval()) >= usia) {
                listUsia.add(tppa);
            }
        }
        tppaView.hideLoading();
        return listUsia;
    }
}
