package com.imaisnaini.daycarereport.ui.fragment;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.imaisnaini.daycarereport.R;
import com.imaisnaini.daycarereport.bl.db.model.Guru;
import com.imaisnaini.daycarereport.ui.activity.AddGuruActivity;
import com.imaisnaini.daycarereport.ui.activity.EditProfileActivity;
import com.imaisnaini.daycarereport.ui.adapter.GuruAdapter;
import com.imaisnaini.daycarereport.ui.dialog.DialogBuilder;
import com.imaisnaini.daycarereport.ui.presenter.GuruPresenter;
import com.imaisnaini.daycarereport.ui.view.GuruView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListGuruFragment extends Fragment implements GuruView {

    @BindView(R.id.rvGuru_guru)
    RecyclerView rvGuru;

    //Activity context;

    private List<Guru> guruList = new ArrayList<>();
    private GuruPresenter guruPresenter;
    private GuruAdapter guruAdapter;
    private MaterialDialog dialog;

    public ListGuruFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_list_guru, container, false);
        ButterKnife.bind(this, view);
        init();
        return view;
    }

    private void init(){
        guruPresenter = new GuruPresenter(getContext(), this);
        guruAdapter =  new GuruAdapter(getContext());

        rvGuru.setHasFixedSize(true);
        rvGuru.setLayoutManager(new LinearLayoutManager(getContext()));
    }

//    public void onStart(){
//        super.onStart();
//        Button button=(Button)context.findViewById(R.id.listguru_fab);
//        button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(context, AddGuruActivity.class);
//                startActivity(intent);
//            }
//        });
//    }
    @Override
    public void onResume() {
        super.onResume();
        guruPresenter.load();
    }

    @Override
    public void showLoading() {
        dialog = DialogBuilder.showLoadingDialog(getContext(), "Updating Data", "Please Wait", false);
    }

    @Override
    public void hideLoading() {
        dialog.dismiss();
    }



    @Override
    public void loadGuru(List<Guru> guruList) {
        guruAdapter.generate(guruList);
        guruAdapter.notifyDataSetChanged();
        rvGuru.setAdapter(guruAdapter);
    }

    @OnClick (R.id.listguru_fab) void goAddGuru(){
        Intent intent = new Intent(getContext(), AddGuruActivity.class);
        startActivity(intent);
    }



}


