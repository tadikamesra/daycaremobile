package com.imaisnaini.daycarereport.ui.view;

import com.imaisnaini.daycarereport.bl.db.model.Guru;
import com.imaisnaini.daycarereport.bl.db.model.Murid;

import java.util.List;

public interface GuruView {
    void showLoading();
    void hideLoading();
    void loadGuru(List<Guru> guruList);
}
