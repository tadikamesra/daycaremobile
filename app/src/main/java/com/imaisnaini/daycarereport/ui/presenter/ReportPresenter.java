package com.imaisnaini.daycarereport.ui.presenter;

import com.imaisnaini.daycarereport.bl.db.dao.ReportDao;
import com.imaisnaini.daycarereport.bl.db.model.Report;
import com.imaisnaini.daycarereport.ui.view.ReportView;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ReportPresenter {
    private ReportView mReportView;

    public ReportPresenter() {
    }

    public ReportPresenter(ReportView mReportView) {
        this.mReportView = mReportView;
    }

    public List<Report> getTodayReport(int idMurid){
        java.sql.Date tgl = new java.sql.Date(System.currentTimeMillis());
        List<Report> list = new ArrayList<>();
        mReportView.showLoading();
        try {
            list = ReportDao.getReportDao().getByTgl(tgl.toString(), idMurid);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        mReportView.hideLoading();
        return list;
    }

    public List<Report> getByDate(String tgl, int idMurid){
        List<Report> list = new ArrayList<>();
        mReportView.showLoading();
        try {
            list = ReportDao.getReportDao().getByTgl(tgl, idMurid);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        mReportView.hideLoading();
        return list;
    }

    public Report getActiveReport(int idMurid, int idKegiatan){
        java.sql.Date tgl = new java.sql.Date(System.currentTimeMillis());
        List<Report> list = new ArrayList<>();
        Report activeReport = null;
        try {
            list = ReportDao.getReportDao().getByTgl(tgl.toString(), idMurid);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        for (Report report : list){
            if (report.getId_kegiatan() == idKegiatan){
                activeReport = report;
            }
        }
        return activeReport;
    }
}
