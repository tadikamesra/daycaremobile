package com.imaisnaini.daycarereport.ui.fragment;


import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.material.textfield.TextInputEditText;
import com.imaisnaini.daycarereport.R;
import com.imaisnaini.daycarereport.bl.network.api.Api;
import com.imaisnaini.daycarereport.bl.network.config.RetrofitBuilder;
import com.imaisnaini.daycarereport.bl.network.model.UserGuru;
import com.imaisnaini.daycarereport.ui.activity.MainActivity;
import com.imaisnaini.daycarereport.ui.dialog.DialogBuilder;
import com.imaisnaini.daycarereport.ui.util.EmailValidator;
import com.imaisnaini.daycarereport.ui.util.PasswordValidator;
import com.imaisnaini.daycarereport.ui.util.PrefUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.imaisnaini.daycarereport.ui.util.Util.isEempty;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginGuruFragment extends Fragment {
    @BindView(R.id.login_guru_tietEmail)
    TextInputEditText etEmail;
    @BindView(R.id.login_guru_tietPassword)
    TextInputEditText etPassword;

    EmailValidator emailValidator;
    PasswordValidator passwordValidator;

    private String email;
    private String password;
    private Api mApi;

    public LoginGuruFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // if user logged in move to main activity
        if (isSessionLogin()){
            startActivity(new Intent(getActivity(), MainActivity.class));
            getActivity().finish();
        }

        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_login_guru, container, false);
        ButterKnife.bind(this, view);
        mApi = RetrofitBuilder.builder(getActivity()).create(Api.class);
        return view;
    }

    boolean isEmail(EditText text){
        emailValidator = new EmailValidator();
        String email = text.getText().toString();
        return emailValidator.isValid(email);
    }

    boolean isPassword(EditText text){
        passwordValidator = new PasswordValidator();
        String pass = text.getText().toString();
        return passwordValidator.isValid(pass);
    }

    @OnClick(R.id.login_guru_btnLogin) void onLogin(){
        if(isEempty(etEmail)){
            etEmail.setError("Email harus diisi");
        }else if(isEempty(etPassword)){
            etPassword.setError("Password harus diisi");
        }else if(!isEmail(etEmail)){
            etEmail.setError("Email tidak valid");
        }else if(!isPassword(etPassword)){
            String str = passwordValidator.getString();
            Toast.makeText(getActivity(),str, Toast.LENGTH_SHORT).show();
        }else {
            loginAct();
        }
    }
    void loginAct(){
        email = etEmail.getText().toString();
        password = etPassword.getText().toString();
        final MaterialDialog dialog = DialogBuilder.showLoadingDialog(getContext(), "Updating Data", "Please wait..", false);
        mApi.loginGuru(email, password).enqueue(new Callback<UserGuru>() {
            @Override
            public void onResponse(Call<UserGuru> call, Response<UserGuru> response) {
                UserGuru user = response.body();
                Log.i("USER_LOGIN", response.message());

                if (user != null){
                    if (!user.getError()){
                        PrefUtil.putGuru(getActivity(), PrefUtil.USER_SESSION, user);
                        Intent intent = new Intent(getActivity(), MainActivity.class);
                        startActivity(intent);
                        getActivity().finish();
                    }
                    Toast.makeText(getActivity(), user.getMessage(), Toast.LENGTH_SHORT).show();
                }
                if (response.code() == 403){
                    etPassword.requestFocus();
                    etPassword.setError(getString(R.string.error_password));
                }
                if (response.code() == 404){
                    etEmail.requestFocus();
                    etEmail.setError(getString(R.string.error_login));
                }
                dialog.dismiss();
            }

            @Override
            public void onFailure(Call<UserGuru> call, Throwable t) {
                //Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
                dialog.dismiss();
                Log.i("USER_LOGIN", t.getMessage());
                DialogBuilder.showErrorDialog(getActivity(), "Gagal Login");
            }
        });
    }

    // this method to check is user logged in ?
    boolean isSessionLogin(){
        return PrefUtil.getGuru(getActivity(), PrefUtil.USER_SESSION) != null;
    }
}
