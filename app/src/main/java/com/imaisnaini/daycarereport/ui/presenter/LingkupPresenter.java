package com.imaisnaini.daycarereport.ui.presenter;

import android.content.Context;

import com.imaisnaini.daycarereport.bl.db.dao.LingkupDao;
import com.imaisnaini.daycarereport.bl.db.model.Lingkup;
import com.imaisnaini.daycarereport.ui.view.LingkupView;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class LingkupPresenter {
    Context context;
    private LingkupView lingkupView;

    public LingkupPresenter(Context context, LingkupView lingkupView) {
        this.context = context;
        this.lingkupView = lingkupView;
    }

    public Lingkup getLingkup(int id){
        Lingkup lingkup = new Lingkup();
        try {
            lingkup = LingkupDao.getLingkupDao().getByID(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lingkup;
    }

    public List<Lingkup> getParent(){
        List<Lingkup> list = new ArrayList<>();
        lingkupView.showLoading();
        try {
            list = LingkupDao.getLingkupDao().getParent();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        lingkupView.hideLoading();
        //lingkupView.loadTppa(list);
        return list;
    }

    public List<Lingkup> getChild(int parent, int usia){
        List<Lingkup> list = new ArrayList<>();
        List<Lingkup> listID = new ArrayList<>();

        lingkupView.showLoading();
        try {
            listID = LingkupDao.getLingkupDao().getChild(parent);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        for (Lingkup lingkup :listID){
            if (lingkup.getUsia() <= usia && (lingkup.getUsia()+lingkup.getInterval())>= usia){
                list.add(lingkup);
            }
        }
        lingkupView.hideLoading();
        return list;
    }
}
