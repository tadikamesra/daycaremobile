package com.imaisnaini.daycarereport.ui.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import com.afollestad.materialdialogs.MaterialDialog;
import com.imaisnaini.daycarereport.bl.db.model.Kegiatan;
import com.imaisnaini.daycarereport.bl.db.model.Murid;
import com.imaisnaini.daycarereport.bl.db.model.Report;
import com.imaisnaini.daycarereport.bl.network.api.Api;
import com.imaisnaini.daycarereport.bl.network.api.SyncWorker;
import com.imaisnaini.daycarereport.bl.network.config.RetrofitBuilder;
import com.imaisnaini.daycarereport.ui.DialogBoxSiangBersihdiriActivity;
import com.imaisnaini.daycarereport.ui.DialogBoxSiangCuciActivity;
import com.imaisnaini.daycarereport.ui.DialogBoxSiangMakanActivity;
import com.imaisnaini.daycarereport.ui.DialogBoxSiangTidurActivity;
import com.imaisnaini.daycarereport.R;
import com.imaisnaini.daycarereport.ui.adapter.KegiatanAdapter;
import com.imaisnaini.daycarereport.ui.presenter.KegiatanPresenter;
import com.imaisnaini.daycarereport.ui.presenter.ReportPresenter;
import com.imaisnaini.daycarereport.ui.view.KegiatanView;
import com.imaisnaini.daycarereport.ui.view.ReportView;
import com.jakewharton.threetenabp.AndroidThreeTen;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class KegiatanSiangActivity extends AppCompatActivity implements KegiatanView, ReportView {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.rvKegiatanSiang)
    RecyclerView rvContent;

    public static int idMurid = 0;
    private int idKegiatan;
    private Murid murid;
    private List<Kegiatan> kegiatanList = new ArrayList<>();
    private List<Report> reportList = new ArrayList<>();
    private MaterialDialog materialDialog;
    private Api mApi;
    private KegiatanAdapter mKegiatanAdapter;
    private KegiatanPresenter mKegiatanPresenter;
    private ReportPresenter mReportPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kegiatan_siang);
        ButterKnife.bind(this);
        AndroidThreeTen.init(this);
        mApi = RetrofitBuilder.builder(this).create(Api.class);
        initData();
        initViews();
    }

    private void initData(){
        idKegiatan = getIntent().getIntExtra("idKegiatan", 0);
        idMurid = getIntent().getIntExtra("idMurid", 0);

        mKegiatanPresenter = new KegiatanPresenter(this);
        kegiatanList = mKegiatanPresenter.getChild(idKegiatan);
        mKegiatanAdapter = new KegiatanAdapter(this, kegiatanList);
    }

    private void initViews() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        if (idKegiatan == 4){
            getSupportActionBar().setTitle("Kegiatan Siang");
        }else {
            getSupportActionBar().setTitle("Kegiatan Sore");
        }

        rvContent.setHasFixedSize(true);
        rvContent.setLayoutManager(new LinearLayoutManager(this));
        rvContent.setAdapter(mKegiatanAdapter);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onPostResume() {
        SyncWorker.getSyncWorker().syncReport(getApplicationContext(), mApi.getReport(), false);
        super.onPostResume();
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void loadReport(List<Report> reportList) {

    }

    @Override
    public void loadKegiatan(List<Kegiatan> kegiatanList) {

    }
}
