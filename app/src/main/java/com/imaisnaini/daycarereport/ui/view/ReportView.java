package com.imaisnaini.daycarereport.ui.view;

import com.imaisnaini.daycarereport.bl.db.model.Report;

import java.util.List;

public interface ReportView {
    void showLoading();
    void hideLoading();
    void loadReport(List<Report> reportList);
}
