package com.imaisnaini.daycarereport.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import com.imaisnaini.daycarereport.R;
import com.imaisnaini.daycarereport.bl.db.model.Lingkup;

import java.util.ArrayList;
import java.util.List;

public class LingkupAdapter extends BaseAdapter {
    private Context context;
    private List<Lingkup> mLingkupList = new ArrayList<>();

    public LingkupAdapter(Context context) {
        this.context = context;
    }

    public LingkupAdapter(Context context, List<Lingkup> mLingkupList) {
        this.context = context;
        this.mLingkupList = mLingkupList;
    }


    @Override
    public int getCount() {
        return mLingkupList.size();
    }

    @Override
    public Object getItem(int i) {
        return mLingkupList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return mLingkupList.get(i).getId_lingkup();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.item_list, null);
        TextView tvTitle = (TextView)view.findViewById(R.id.tvTitle_itemDialogList);
        tvTitle.setText(mLingkupList.get(i).getLingkup());
        return view;
    }
}
