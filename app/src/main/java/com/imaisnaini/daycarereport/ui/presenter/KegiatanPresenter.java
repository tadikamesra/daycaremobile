package com.imaisnaini.daycarereport.ui.presenter;

import android.content.Context;

import com.imaisnaini.daycarereport.bl.db.dao.KegiatanDao;
import com.imaisnaini.daycarereport.bl.db.model.Kegiatan;
import com.imaisnaini.daycarereport.ui.view.KegiatanView;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class KegiatanPresenter {
    Context context;
    private KegiatanView mKegiatanView;

    public KegiatanPresenter(KegiatanView mKegiatanView) {
        this.mKegiatanView = mKegiatanView;
    }

    public KegiatanPresenter(Context context, KegiatanView mKegiatanView) {
        this.context = context;
        this.mKegiatanView = mKegiatanView;
    }

    public List<Kegiatan> getChild(int parent){
        List<Kegiatan> list = new ArrayList<>();
        mKegiatanView.showLoading();
        try {
            list = KegiatanDao.getKegiatanDao().getByParent(parent);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        mKegiatanView.hideLoading();
        return list;
    }
}
