package com.imaisnaini.daycarereport.ui.presenter;

import android.content.Context;

import com.imaisnaini.daycarereport.bl.db.dao.LingkupDao;
import com.imaisnaini.daycarereport.bl.db.dao.OrtuDao;
import com.imaisnaini.daycarereport.bl.db.model.Lingkup;
import com.imaisnaini.daycarereport.bl.db.model.Ortu;
import com.imaisnaini.daycarereport.ui.view.OrtuView;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class OrtuPresenter {
    Context context;
    private OrtuView ortuView;

    public OrtuPresenter(Context context, OrtuView ortuView) {
        this.context = context;
        this.ortuView = ortuView;
    }

    public void readAll(){
        ortuView.showLoading();
        List<Ortu> list = new ArrayList<>();
        try {
            list = OrtuDao.getOrtuDao().read();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ortuView.loadOrtu(list);
        ortuView.hideLoading();
    }

    public Ortu getByID(int id){
        Ortu ortu = null;
        try {
            ortu = OrtuDao.getOrtuDao().getByID(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return ortu;
    }

}
