package com.imaisnaini.daycarereport.ui.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.imaisnaini.daycarereport.bl.network.config.Config;
import com.imaisnaini.daycarereport.bl.network.model.UserGuru;
import com.imaisnaini.daycarereport.bl.network.model.UserOrtu;

public class PrefUtil {
  public static final String USER_SESSION = "user_session";
  public static final String USER_STORAGE = "user_storage";

  public static SharedPreferences getSharedPreferences(Context ctx){
    return PreferenceManager.getDefaultSharedPreferences(ctx);
  }

  public static void putGuru(Context ctx, String key, UserGuru user){
    Gson gson = new Gson();
    String json = gson.toJson(user);
    putString(ctx, key, json);
  }

  public static void putOrtu(Context ctx, String key, UserOrtu user){
    Gson gson = new Gson();
    String json = gson.toJson(user);
    putString(ctx, key, json);
  }

  public static UserGuru getGuru(Context ctx, String key){
    Gson gson = new Gson();
    String json = getString(ctx, key);
    UserGuru user = gson.fromJson(json, UserGuru.class);
    return user;
  }

  public static UserOrtu getOrtu(Context ctx, String key){
    Gson gson = new Gson();
    String json = getString(ctx, key);
    UserOrtu user = gson.fromJson(json, UserOrtu.class);
    return user;
  }

  public static void putString(Context ctx, String key, String value){
    getSharedPreferences(ctx).edit().putString(key, value).apply();
  }

  public static String getString(Context ctx, String key){
    return getSharedPreferences(ctx).getString(key, null);
  }

  public static void clear(Context ctx) {
    getSharedPreferences(ctx).edit().clear().apply();
  }
}
