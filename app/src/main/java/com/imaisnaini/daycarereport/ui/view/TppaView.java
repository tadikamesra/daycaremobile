package com.imaisnaini.daycarereport.ui.view;

import com.imaisnaini.daycarereport.bl.db.model.Tppa;

import java.util.List;

public interface TppaView {
    void showLoading();
    void hideLoading();
    void loadTppa(List<Tppa> tppaList);
}
