package com.imaisnaini.daycarereport.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.imaisnaini.daycarereport.R;
import com.imaisnaini.daycarereport.bl.db.model.Guru;
import com.imaisnaini.daycarereport.bl.db.model.Tppa;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TppaAdapter extends RecyclerView.Adapter<TppaAdapter.ViewHolder> {
    Context context;
    private List<Tppa> mList =  new ArrayList<>();
    private List<Tppa> tppaChecked =  new ArrayList<>();

    public TppaAdapter(Context context) {
        this.context = context;
    }

    public TppaAdapter(Context context, List<Tppa> mList) {
        this.context = context;
        this.mList = mList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_tppa, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.cbTppa.setText(mList.get(position).getKeterangan());
        holder.cbTppa.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                    if (isChecked){
                        tppaChecked.add(mList.get(position));
                    }else {
                        tppaChecked.remove(mList.get(position));
                    }
                }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.cbTppa_itemTppa)
        CheckBox cbTppa;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void generate(List<Tppa> list){
        clear();
        mList = list;
        notifyDataSetChanged();
    }

    public void clear(){
        mList.clear();
        notifyDataSetChanged();
    }

    public List<Tppa> getTppaChecked(){
        return tppaChecked;
    }
}
