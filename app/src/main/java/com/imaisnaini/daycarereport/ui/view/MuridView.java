package com.imaisnaini.daycarereport.ui.view;

import com.imaisnaini.daycarereport.bl.db.model.Murid;

import java.util.List;

public interface MuridView {
    void showLoading();
    void hideLoading();
    void loadMurid(List<Murid> muridList);
}
