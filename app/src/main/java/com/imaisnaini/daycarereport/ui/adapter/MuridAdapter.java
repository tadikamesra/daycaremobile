package com.imaisnaini.daycarereport.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.imaisnaini.daycarereport.R;
import com.imaisnaini.daycarereport.bl.db.model.Murid;
import com.imaisnaini.daycarereport.ui.activity.ListKegiatanGuruActivity;
import com.jakewharton.threetenabp.AndroidThreeTen;

import org.threeten.bp.LocalDate;
import org.threeten.bp.Period;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MuridAdapter extends RecyclerView.Adapter<MuridAdapter.ViewHolder> {
    Context context;
    List<Murid> list = new ArrayList<>();

    public MuridAdapter(Context context) {
        this.context = context;
        AndroidThreeTen.init(context);
    }

    public MuridAdapter(Context context, List<Murid> list) {
        this.context = context;
        this.list = list;
        AndroidThreeTen.init(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_murid, parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Murid murid = list.get(position);

        holder.id = murid.getId_murid();
        holder.tvNama.setText(murid.getNama());
        holder.tvUmur.setText(murid.getUsia() + " bulan");

        if (murid.getJk().equals("L")){
            if (murid.getUsia() <= 12)
                holder.ivAvatar.setImageResource(R.drawable.ic_baby_boy);
            else if (murid.getUsia() <= 48)
                holder.ivAvatar.setImageResource(R.drawable.ic_toddler_boy);
            else
                holder.ivAvatar.setImageResource(R.drawable.ic_student_boy);
        }else {
            if (murid.getUsia() <= 12)
                holder.ivAvatar.setImageResource(R.drawable.ic_baby_girl);
            else if (murid.getUsia() <= 48)
                holder.ivAvatar.setImageResource(R.drawable.ic_toddler_girl);
            else
                holder.ivAvatar.setImageResource(R.drawable.ic_student_girl);

        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.tvNama_murid)
        TextView tvNama;
        @BindView(R.id.ivAvatar_murid)
        ImageView ivAvatar;
        @BindView(R.id.tvUmur_murid)
        TextView tvUmur;
        int id;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.cardview_murid) void onClick(){
            Intent intent = new Intent(context, ListKegiatanGuruActivity.class);
            intent.putExtra("id", id);
            context.startActivity(intent);
        }
    }

    public void generate(List<Murid> list) {
        clear();
        this.list = list;
        notifyDataSetChanged();
    }

    public void clear() {
        list.clear();
        notifyDataSetChanged();
    }
}
