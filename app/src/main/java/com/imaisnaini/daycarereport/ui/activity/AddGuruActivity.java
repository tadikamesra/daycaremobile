package com.imaisnaini.daycarereport.ui.activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.renderscript.RenderScript;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.material.textfield.TextInputEditText;
import com.imaisnaini.daycarereport.R;
import com.imaisnaini.daycarereport.bl.db.model.Guru;
import com.imaisnaini.daycarereport.bl.network.api.Api;
import com.imaisnaini.daycarereport.bl.network.api.SyncWorker;
import com.imaisnaini.daycarereport.bl.network.config.RetrofitBuilder;
import com.imaisnaini.daycarereport.bl.network.model.BaseRespons;
import com.imaisnaini.daycarereport.bl.network.model.UserGuru;
import com.imaisnaini.daycarereport.ui.dialog.DialogBuilder;
import com.imaisnaini.daycarereport.ui.presenter.GuruPresenter;
import com.imaisnaini.daycarereport.ui.util.PrefUtil;
import com.imaisnaini.daycarereport.ui.view.GuruView;
import com.jakewharton.threetenabp.AndroidThreeTen;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddGuruActivity extends AppCompatActivity implements GuruView {

        private int id_guru;
        private GuruPresenter guruPresenter;
        private Guru guru;
        private UserGuru user;
        private Api mApi;
        private MaterialDialog mDialog;
        private Context ctx;
        private String selected_jk;

        @BindView(R.id.texfil_addguru_nama)
        TextInputEditText etNama;
        @BindView(R.id.texfil_addguru_nip)
        TextInputEditText etNip;
        @BindView(R.id.texfil_addguru_email)
        TextInputEditText etEmail;
        @BindView(R.id.sp_daftarJk_addGuru)
        Spinner spJk;
//        @BindView(R.id.texfil_addguru_jk)
//        TextInputEditText etJk;
        @BindView(R.id.texfil_addguru_telepon)
        TextInputEditText etTelp;
        @BindView(R.id.texfil_addguru_password)
        TextInputEditText etPassword;
        @BindView(R.id.btn_cancel_addguru)
        Button btn_cancel;
        @BindView(R.id.btn_ok_addguru)
        Button btn_ok;
        @BindView(R.id.toolbar)
        Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_guru);
        ButterKnife.bind(this);
        AndroidThreeTen.init(this);
        initData();
        initViews();

//        etNama.findViewById(R.id.texfil_addguru_nama);
//        etNip.findViewById(R.id.texfil_addguru_nip);
//        etEmail.findViewById(R.id.texfil_addguru_email);
//        etJk.findViewById(R.id.texfil_addguru_jk);
//        etTelp.findViewById(R.id.texfil_addguru_telepon);
//        etPassword.findViewById(R.id.texfil_addguru_password);
    }

    private void initData(){
        mApi = RetrofitBuilder.builder(this).create(Api.class);
        id_guru = getIntent().getIntExtra("id_guru", 0);
        user = PrefUtil.getGuru(this, PrefUtil.USER_SESSION);
        guruPresenter = new GuruPresenter(getApplicationContext(), this);
        guru = guruPresenter.getByID(user.getIdGuru());
    }

    private void initViews(){

        toolbar.setTitle("Tambah Guru");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        if(user.getPrevillage()==1){
            btn_ok.setVisibility(View.VISIBLE);
        } else {

        }


//        etNama.setText(String.valueOf(guru.getNama()));
//        etNip.setText(String.valueOf(guru.getNip()));
//        etEmail.setText(String.valueOf(guru.getEmail()));
//        etJk.setText(String.valueOf(guru.getJk()));
//        etTelp.setText(String.valueOf(guru.getTelp()));
//
//        etNama.findViewById(R.id.texfil_addguru_nama);
//        etNip.findViewById(R.id.texfil_addguru_nip);
       }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @OnItemSelected(R.id.sp_daftarJk_addGuru) void jkSelected(int position){
        selected_jk = (String) spJk.getAdapter().getItem(position);
    }

    @OnItemSelected(value = R.id.sp_daftarJk_addGuru, callback = OnItemSelected.Callback.NOTHING_SELECTED)
    void onNothingSelected() {

    }

    @OnClick(R.id.btn_ok_addguru) void onAdd(){
        MaterialDialog dialog = DialogBuilder.showLoadingDialog(AddGuruActivity.this, "Add Data", "Please Wait", false);
        mApi.addGuru(etNama.getText().toString(),
                etNip.getText().toString(),etEmail.getText().toString(),
                selected_jk,etTelp.getText().toString(), etPassword.getText().toString())
                .enqueue(new Callback<BaseRespons>() {
                    @Override
                    public void onResponse(Call<BaseRespons> call, Response<BaseRespons> response) {
                        BaseRespons baseRespons = response.body();
                        String message = "";
                        if (baseRespons!=null){
                            if(!baseRespons.getError()){
                                message = "Data berhasil ditambahkan";
                                SyncWorker.getSyncWorker().syncGuru(ctx, mApi.getGuru(), false);
                            }
                        }
                        if (response.code() == 400){
                            message = "Invalid parameter!";
                        }
                        if (response.code() == 502){
                            message = "Gagal menyimpan ke database.";
                        }
                        Toast.makeText(AddGuruActivity.this, message, Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                        try {
                            Thread.sleep(3000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        onSupportNavigateUp();
                    }

                    @Override
                    public void onFailure(Call<BaseRespons> call, Throwable t) {

                    }
                });
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void loadGuru(List<Guru> guruList) {

    }


}
