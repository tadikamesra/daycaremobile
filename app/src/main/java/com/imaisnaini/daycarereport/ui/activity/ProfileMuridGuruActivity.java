package com.imaisnaini.daycarereport.ui.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.imaisnaini.daycarereport.R;
import com.imaisnaini.daycarereport.bl.db.model.Murid;
import com.imaisnaini.daycarereport.bl.db.model.Ortu;
import com.imaisnaini.daycarereport.ui.presenter.MuridPresenter;
import com.imaisnaini.daycarereport.ui.presenter.OrtuPresenter;
import com.imaisnaini.daycarereport.ui.view.MuridView;
import com.imaisnaini.daycarereport.ui.view.OrtuView;
import com.jakewharton.threetenabp.AndroidThreeTen;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProfileMuridGuruActivity extends AppCompatActivity implements MuridView, OrtuView {

    private int id_murid, id_ortu;
    private OrtuPresenter ortuPresenter;
    private MuridPresenter muridPresenter;
    private Murid murid;
    private Ortu ortu;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tv_nama_profilmurid)
    TextView tvNama;
    @BindView(R.id.tv_berat_profilmurid) TextView tvBB;
    @BindView(R.id.tv_tinggi_profilmurid) TextView tvTB;
    @BindView(R.id.tv_namaortu_profilmurid) TextView tvNamaOrtu;
    @BindView(R.id.tv_telportu_profilmurid) TextView tvTelpOrtu;
    @BindView(R.id.tv_alamatortu_profilmurid) TextView tvAlamatOrtu;
    @BindView(R.id.iv_profilmurid)
    ImageView ivAvatar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_murid_guru);
        ButterKnife.bind(this);
        AndroidThreeTen.init(this);
        initData();
        initViews();
    }

    private void initData(){
        id_murid = getIntent().getIntExtra("id", 0);
        id_ortu = getIntent().getIntExtra("id_ortu", 0);
        muridPresenter = new MuridPresenter(getApplicationContext(), this);
        murid = muridPresenter.getByID(id_murid);
        ortuPresenter = new OrtuPresenter(getApplicationContext(),this);
        ortu = ortuPresenter.getByID(id_ortu);
    }

    private void initViews(){
        toolbar.setTitle("Profil Murid");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        tvBB.setText(String.valueOf(murid.getBb()));
        tvTB.setText(String.valueOf(murid.getTb()));
        tvNama.setText(String.valueOf(murid.getNama()));
        tvNamaOrtu.setText(String.valueOf(ortu.getNama()));
        tvAlamatOrtu.setText(String.valueOf(ortu.getAlamat()));
        tvTelpOrtu.setText(String.valueOf(ortu.getTelp()));

        // Set Avatar sesuai jenis kelamin dan umur
        if (murid.getJk().equals("L")){
            if (murid.getUsia() <= 12)
                ivAvatar.setImageResource(R.drawable.ic_baby_boy);
            else if (murid.getUsia() <= 48)
                ivAvatar.setImageResource(R.drawable.ic_toddler_boy);
            else
                ivAvatar.setImageResource(R.drawable.ic_student_boy);
        }else {
            if (murid.getUsia() <= 12)
                ivAvatar.setImageResource(R.drawable.ic_baby_girl);
            else if (murid.getUsia() <= 48)
                ivAvatar.setImageResource(R.drawable.ic_toddler_girl);
            else
                ivAvatar.setImageResource(R.drawable.ic_student_girl);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void loadOrtu(List<Ortu> ortuList) {

    }

    @Override
    public void loadMurid(List<Murid> muridList) {

    }
}
