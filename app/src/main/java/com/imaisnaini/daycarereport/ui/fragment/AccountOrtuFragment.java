package com.imaisnaini.daycarereport.ui.fragment;


import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.imaisnaini.daycarereport.R;
import com.imaisnaini.daycarereport.bl.db.helper.Db;
import com.imaisnaini.daycarereport.bl.db.model.Ortu;
import com.imaisnaini.daycarereport.bl.network.api.Api;
import com.imaisnaini.daycarereport.bl.network.config.RetrofitBuilder;
import com.imaisnaini.daycarereport.bl.network.model.UserOrtu;
import com.imaisnaini.daycarereport.ui.activity.EditProfileOrtuActivity;
import com.imaisnaini.daycarereport.ui.activity.LoginActivity;
import com.imaisnaini.daycarereport.ui.presenter.OrtuPresenter;
import com.imaisnaini.daycarereport.ui.util.PrefUtil;
import com.imaisnaini.daycarereport.ui.view.OrtuView;
import com.j256.ormlite.stmt.query.In;
import com.jakewharton.threetenabp.AndroidThreeTen;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class AccountOrtuFragment extends Fragment implements OrtuView {

    private int id_ortu;
    private OrtuPresenter ortuPresenter;
    private Ortu ortu;
    private UserOrtu user;
    private Api mApi;

    @BindView(R.id.tv_nama_profilortu)
    TextView tvNama;
    @BindView(R.id.tv_nik_profileortu) TextView tvNik;
    @BindView(R.id.tv_jk_profileortu) TextView tvJk;
    @BindView(R.id.tv_telp_profileortu) TextView tvTelp;
    @BindView(R.id.tv_alamat_profileortu) TextView tvAlamat;
    @BindView(R.id.iv_profilortu)
    ImageView ivAvatar;

    public AccountOrtuFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_account_ortu, container, false);
        ButterKnife.bind(this,view);
        Db.getInstance().init(getActivity());
        AndroidThreeTen.init(getActivity());
        mApi = RetrofitBuilder.builder(getActivity()).create(Api.class);
        initData();
        initViews();
        return view;
    }

    private void initData(){
        user = PrefUtil.getOrtu(getActivity(), PrefUtil.USER_SESSION);
        ortuPresenter = new OrtuPresenter(getActivity().getApplicationContext(), this);
        ortu = ortuPresenter.getByID(user.getIdOrtu());
    }


    private void initViews(){
        tvNama.setText(String.valueOf(ortu.getNama()));
        tvNik.setText(String.valueOf(ortu.getNik()));
        tvJk.setText(String.valueOf(ortu.getJk()));
        tvTelp.setText(String.valueOf(ortu.getTelp()));
        tvAlamat.setText(String.valueOf(ortu.getAlamat()));
        ivAvatar.setImageResource(R.drawable.profile);

    }

    @OnClick(R.id.ortu_btnLogout) void doLogout(){
        PrefUtil.clear(getActivity());
        startActivity(new Intent(getContext(), LoginActivity.class));
        getActivity().finish();
    }

    @OnClick(R.id.btn_edit_profil_ortu) void goEdit(){
        Intent intent = new Intent(getContext(), EditProfileOrtuActivity.class);
        intent.putExtra("id_ortu", user.getIdOrtu());
        startActivity (intent);
        //startActivity(new Intent(getContext(), EditProfileActivity.class));
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void loadOrtu(List<Ortu> ortuList) {

    }
}
