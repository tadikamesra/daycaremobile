package com.imaisnaini.daycarereport.ui.fragment;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.tabs.TabLayout;
import com.imaisnaini.daycarereport.R;
import com.imaisnaini.daycarereport.bl.db.helper.Db;
import com.imaisnaini.daycarereport.bl.db.model.Guru;
import com.imaisnaini.daycarereport.bl.network.model.UserGuru;
import com.imaisnaini.daycarereport.event.MuridTabEvent;
import com.imaisnaini.daycarereport.ui.activity.AddMuridActivity;
import com.imaisnaini.daycarereport.ui.activity.DetailOrtuActivity;
import com.imaisnaini.daycarereport.ui.activity.KegiatanSiangActivity;
import com.imaisnaini.daycarereport.ui.activity.ListKegiatanGuruActivity;
import com.imaisnaini.daycarereport.ui.adapter.TabMuridAdapter;
import com.imaisnaini.daycarereport.ui.presenter.GuruPresenter;
import com.jakewharton.threetenabp.AndroidThreeTen;

import org.greenrobot.eventbus.EventBus;

import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * A simple {@link Fragment} subclass.
 */
public class ListMuridFragment extends Fragment {

    View myFragment;
    ViewPager viewPager;
    TabLayout tabLayout;
    private int id_guru;
    private GuruPresenter guruPresenter;
    private Guru guru;
    private UserGuru user;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        myFragment = inflater.inflate(R.layout.fragment_list_murid, container, false);
        ButterKnife.bind(this, myFragment);
        Db.getInstance().init(getActivity());
        AndroidThreeTen.init(getActivity());
        viewPager = myFragment.findViewById(R.id.listmurid_vp);
        tabLayout = myFragment.findViewById(R.id.listmurid_tablayout);

        return myFragment;
    }

    //call onActivity create method

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        init();
        initListener();
    }

    private void init(){
        TabMuridAdapter adapter = new TabMuridAdapter(getChildFragmentManager());

        adapter.addFragment(new MuridsFragment(), "0-12");
        adapter.addFragment(new MuridsFragment(), "12-24");
        adapter.addFragment(new MuridsFragment(), "24-48");
        adapter.addFragment(new MuridsFragment(), "48-72");

        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void initListener(){
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                EventBus.getDefault().postSticky(new MuridTabEvent(position));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                EventBus.getDefault().postSticky(new MuridTabEvent(tab.getPosition()));
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @OnClick(R.id.listmurid_fab) void goAddMurid(){
        Intent intent = new Intent(getContext(), DetailOrtuActivity.class);
        //intent.putExtra("id_ortu", user.getIdOrtu());
        startActivity (intent);
    }

//    public void goAddMurid(View view){
//        Intent intent = new Intent(getContext(), DetailOrtuActivity.class);
////        //intent.putExtra("id_ortu", user.getIdOrtu());
//        startActivity (intent);
//    }
}
