package com.imaisnaini.daycarereport.ui.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.content.Context;
import android.os.AsyncTask;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.imaisnaini.daycarereport.R;
import com.imaisnaini.daycarereport.bl.db.dao.GuruDao;
import com.imaisnaini.daycarereport.bl.db.dao.KegiatanDao;
import com.imaisnaini.daycarereport.bl.db.dao.LingkupDao;
import com.imaisnaini.daycarereport.bl.db.dao.MuridDao;
import com.imaisnaini.daycarereport.bl.db.dao.OrtuDao;
import com.imaisnaini.daycarereport.bl.db.helper.Db;
import com.imaisnaini.daycarereport.bl.db.model.Guru;
import com.imaisnaini.daycarereport.bl.db.model.Kegiatan;
import com.imaisnaini.daycarereport.bl.db.model.Lingkup;
import com.imaisnaini.daycarereport.bl.db.model.Murid;
import com.imaisnaini.daycarereport.bl.db.model.Ortu;
import com.imaisnaini.daycarereport.bl.network.api.Api;
import com.imaisnaini.daycarereport.bl.network.api.SyncWorker;
import com.imaisnaini.daycarereport.bl.network.config.RetrofitBuilder;
import com.imaisnaini.daycarereport.bl.network.model.UserGuru;
import com.imaisnaini.daycarereport.ui.activity.EditProfileActivity;
import com.imaisnaini.daycarereport.ui.activity.LoginActivity;
import com.imaisnaini.daycarereport.ui.activity.MainActivity;
import com.imaisnaini.daycarereport.ui.dialog.DialogBuilder;
import com.imaisnaini.daycarereport.ui.presenter.GuruPresenter;
import com.imaisnaini.daycarereport.ui.presenter.MuridPresenter;
import com.imaisnaini.daycarereport.ui.util.PrefUtil;
import com.imaisnaini.daycarereport.ui.view.GuruView;
import com.jakewharton.threetenabp.AndroidThreeTen;

import java.util.List;
import java.sql.SQLException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class AccountGuruFragment extends Fragment implements GuruView{


    private int id_guru;
    private GuruPresenter guruPresenter;
    private Guru guru;
    private UserGuru user;
    private Api mApi;

    @BindView(R.id.tv_nama_profilguru) TextView tvNama;
    @BindView(R.id.tv_email_profileguru) TextView tvEmail;
    @BindView(R.id.tv_nip_profileguru) TextView tvNip;
    @BindView(R.id.tv_jk_profileguru) TextView tvJk;
    @BindView(R.id.tv_telp_profileguru) TextView tvTelp;
    @BindView(R.id.iv_profilguru)
    ImageView ivAvatar;

    public AccountGuruFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_account_guru, container, false);
        ButterKnife.bind(this,view);
        Db.getInstance().init(getActivity());
        AndroidThreeTen.init(getActivity());
        mApi = RetrofitBuilder.builder(getActivity()).create(Api.class);
        initData();
        initViews();
        return view;
    }

    private void initData(){
        user = PrefUtil.getGuru(getActivity(), PrefUtil.USER_SESSION);
        guruPresenter = new GuruPresenter(getActivity().getApplicationContext(), this);
        guru = guruPresenter.getByID(user.getIdGuru());
    }


    private void initViews(){
        tvNama.setText(String.valueOf(guru.getNama()));
        tvEmail.setText(String.valueOf(guru.getEmail()));
        tvNip.setText(String.valueOf(guru.getNip()));
        tvJk.setText(String.valueOf(guru.getJk()));
        tvTelp.setText(String.valueOf(guru.getTelp()));
        ivAvatar.setImageResource(R.drawable.profile);

    }

    @OnClick(R.id.guru_btnLogout) void doLogout(){
        PrefUtil.clear(getActivity());
        startActivity(new Intent(getContext(), LoginActivity.class));
        getActivity().finish();
    }
    @OnClick(R.id.btn_edit_profile) void goEdit(){
        Intent intent = new Intent(getContext(), EditProfileActivity.class);
        intent.putExtra("id_guru", user.getIdGuru());
        startActivity (intent);
        //startActivity(new Intent(getContext(), EditProfileActivity.class));
    }


    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void loadGuru(List<Guru> guruList) {

    }
}
