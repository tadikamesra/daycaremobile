package com.imaisnaini.daycarereport.ui.activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.material.textfield.TextInputEditText;
import com.imaisnaini.daycarereport.R;
import com.imaisnaini.daycarereport.bl.db.model.Murid;
import com.imaisnaini.daycarereport.bl.db.model.Report;
import com.imaisnaini.daycarereport.bl.network.api.Api;
import com.imaisnaini.daycarereport.bl.network.api.SyncWorker;
import com.imaisnaini.daycarereport.bl.network.config.RetrofitBuilder;
import com.imaisnaini.daycarereport.bl.network.model.BaseRespons;
import com.imaisnaini.daycarereport.bl.network.model.UserGuru;
import com.imaisnaini.daycarereport.ui.SetTime;
import com.imaisnaini.daycarereport.ui.dialog.DialogBuilder;
import com.imaisnaini.daycarereport.ui.presenter.MuridPresenter;
import com.imaisnaini.daycarereport.ui.presenter.ReportPresenter;
import com.imaisnaini.daycarereport.ui.util.PrefUtil;
import com.imaisnaini.daycarereport.ui.view.MuridView;
import com.imaisnaini.daycarereport.ui.view.ReportView;
import com.jakewharton.threetenabp.AndroidThreeTen;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListKegiatanGuruActivity extends AppCompatActivity implements MuridView, ReportView {

    CardView cvKPeriksa, cvBBebas, cvKPagi, cvKSiang, cvKSore;
    Button btnDetail;
    AlertDialog.Builder dialog;
    View dialogView;
    LayoutInflater inflater;

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.tvUmur_kegiatan) TextView tvUmur;
    @BindView(R.id.tvBB_kegiatan) TextView tvBB;
    @BindView(R.id.tvTB_kegiatan) TextView tvTB;
    @BindView(R.id.ivAvatar_kegiatan) ImageView ivAvatar;

    private int id_murid;
    private MuridPresenter muridPresenter;
    private ReportPresenter reportPresenter;
    private Murid murid;
    private Report activeReport;
    private MaterialDialog mDialog;
    private Api mApi;
    private UserGuru user;
    private List<Report> reportList = new ArrayList<>();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_kegiatan_guru);
        ButterKnife.bind(this);
        AndroidThreeTen.init(this);
        mApi = RetrofitBuilder.builder(this).create(Api.class);
        user = PrefUtil.getGuru(this, PrefUtil.USER_SESSION);
        initData();
        initViews();
    }

    private void initData(){
        id_murid = getIntent().getIntExtra("id", 0);
        muridPresenter = new MuridPresenter(getApplicationContext(), this);
        reportPresenter = new ReportPresenter(this);
        murid = muridPresenter.getByID(id_murid);
        reportList = reportPresenter.getTodayReport(id_murid);
    }

    private void initViews(){
        toolbar.setTitle(murid.getNama());
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        cvKPeriksa = (CardView) findViewById(R.id.cv_pemeriksaantubuh_guru);
        cvBBebas = (CardView) findViewById(R.id.cv_bermainbebas_guru);
        cvKPagi = (CardView) findViewById(R.id.cv_kegiatanpagi_guru);
        cvKSiang = (CardView) findViewById(R.id.cv_kegiatansiang_guru);
        cvKSore = (CardView) findViewById(R.id.cv_kegiatansore_guru);

        btnDetail = findViewById(R.id.guru_btnDetailOrtu);

        if (murid.getUsia() >= 12)
            tvUmur.setText("Usia : " + murid.getUsia()/12 + " tahun " + murid.getUsia()%12 + " bulan");
        else
            tvUmur.setText("Usia : " + murid.getUsia() + " bulan");

        tvBB.setText("Tinggi Badan : " + murid.getBb() + " cm");
        tvTB.setText("Berat Badan : " + murid.getTb() + " kg");

        // Set Avatar sesuai jenis kelamin dan umur
        if (murid.getJk().equals("L")){
            if (murid.getUsia() <= 12)
                ivAvatar.setImageResource(R.drawable.ic_baby_boy);
            else if (murid.getUsia() <= 48)
                ivAvatar.setImageResource(R.drawable.ic_toddler_boy);
            else
                ivAvatar.setImageResource(R.drawable.ic_student_boy);
        }else {
            if (murid.getUsia() <= 12)
                ivAvatar.setImageResource(R.drawable.ic_baby_girl);
            else if (murid.getUsia() <= 48)
                ivAvatar.setImageResource(R.drawable.ic_toddler_girl);
            else
                ivAvatar.setImageResource(R.drawable.ic_student_girl);
        }
    }

    @Override
    protected void onPostResume() {
        SyncWorker.getSyncWorker().syncReport(getApplicationContext(), mApi.getReport(), false);
        super.onPostResume();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @OnClick(R.id.cv_pemeriksaantubuh_guru) void showCheckup(){
        mDialog = new MaterialDialog.Builder(ListKegiatanGuruActivity.this)
                .customView(R.layout.activity_dialog_box_kegiatan_suhu, true)
                .positiveText(R.string.simpan)
                .negativeText(R.string.batal)
                .build();

        // inisialisasi view dialog
        TextInputEditText etSuhu = mDialog.getCustomView().findViewById(R.id.tietSuhu_kegiatan);
        TextInputEditText etWaktu = mDialog.getCustomView().findViewById(R.id.tietWaktu_kegiatan);
        SetTime fromTime = new SetTime(etWaktu, ListKegiatanGuruActivity.this);

        activeReport = reportPresenter.getActiveReport(id_murid, 1);
        if (activeReport!=null){
            etSuhu.setText(activeReport.getKeterangan());
            etWaktu.setText(activeReport.getWaktu().substring(0,5));
        }

        mDialog.getActionButton(DialogAction.POSITIVE).setOnClickListener(view -> {
            MaterialDialog dialog = DialogBuilder.showLoadingDialog(ListKegiatanGuruActivity.this, "Saving Data", "Please Wait", false);
            if (activeReport==null) {
                mApi.addReport(murid.getId_murid(), user.getIdGuru(), 1, etSuhu.getText().toString(), fromTime.getTimeResult())
                        .enqueue(new Callback<BaseRespons>() {
                            @Override
                            public void onResponse(Call<BaseRespons> call, Response<BaseRespons> response) {
                                BaseRespons baseRespons = response.body();
                                String message = "";
                                if (baseRespons != null) {
                                    if (!baseRespons.getError()) {
                                        message = "Data berhasil disimpan.";
                                    }
                                }
                                if (response.code() == 400) {
                                    message = "Invalid parameter!";
                                }
                                if (response.code() == 502) {
                                    message = "Gagal menyimpan ke database.";
                                }
                                Toast.makeText(ListKegiatanGuruActivity.this, message, Toast.LENGTH_SHORT).show();
                                dialog.dismiss();
                            }

                            @Override
                            public void onFailure(Call<BaseRespons> call, Throwable t) {

                            }
                        });
            }else {
                mApi.updateReport(activeReport.getId_report(),
                        murid.getId_murid(),
                        user.getIdGuru(),
                        1,
                        etSuhu.getText().toString(),
                        etWaktu.getText().toString()+":00")
                        .enqueue(new Callback<BaseRespons>() {
                            @Override
                            public void onResponse(Call<BaseRespons> call, Response<BaseRespons> response) {
                                BaseRespons baseRespons = response.body();
                                String message = "";
                                if (baseRespons != null) {
                                    if (!baseRespons.getError()) {
                                        message = "Data berhasil diupdate.";
                                    }
                                }
                                if (response.code() == 400) {
                                    message = "Invalid parameter!";
                                }
                                if (response.code() == 502) {
                                    message = "Gagal menyimpan ke database.";
                                }
                                Toast.makeText(ListKegiatanGuruActivity.this, message, Toast.LENGTH_SHORT).show();
                                dialog.dismiss();
                            }

                            @Override
                            public void onFailure(Call<BaseRespons> call, Throwable t) {

                            }
                        });
            }
            SyncWorker.getSyncWorker().syncReport(getApplicationContext(), mApi.getReport(), false);
            mDialog.dismiss();
        });
        mDialog.show();
    }

    @OnClick(R.id.cv_bermainbebas_guru) void showPlay(){
        mDialog = new MaterialDialog.Builder(ListKegiatanGuruActivity.this)
                .customView(R.layout.activity_dialog_box_kegiatan, true)
                .positiveText(R.string.simpan)
                .negativeText(R.string.batal)
                .build();

        TextInputEditText etKeterangan = mDialog.getCustomView().findViewById(R.id.tietKeterangan_dialogKegiatan);
        TextInputEditText etWaktu = mDialog.getCustomView().findViewById(R.id.tietWaktu_dialogKegiatan);
        SetTime fromTime = new SetTime(etWaktu, ListKegiatanGuruActivity.this);

        activeReport = reportPresenter.getActiveReport(id_murid, 2);
        if (activeReport!=null){
            etKeterangan.setText(activeReport.getKeterangan());
            etWaktu.setText(activeReport.getWaktu().substring(0,5));
        }

        mDialog.getActionButton(DialogAction.POSITIVE).setOnClickListener(view -> {
            MaterialDialog dialog = DialogBuilder.showLoadingDialog(ListKegiatanGuruActivity.this, "Saving Data", "Please Wait", false);
            if (activeReport==null) {
                mApi.addReport(id_murid, user.getIdGuru(), 2, etKeterangan.getText().toString(), fromTime.getTimeResult())
                        .enqueue(new Callback<BaseRespons>() {
                            @Override
                            public void onResponse(Call<BaseRespons> call, Response<BaseRespons> response) {
                                BaseRespons baseRespons = response.body();
                                String msg = "";
                                if (baseRespons != null) {
                                    if (!baseRespons.getError()) {
                                        msg = "Data berhasil disimpan.";
                                    }
                                }
                                if (response.code() == 400) {
                                    msg = "Invalid parameter!";
                                }
                                if (response.code() == 502) {
                                    msg = "Gagal menyimpan ke database.";
                                }
                                Toast.makeText(ListKegiatanGuruActivity.this, msg, Toast.LENGTH_SHORT).show();
                                dialog.dismiss();
                            }

                            @Override
                            public void onFailure(Call<BaseRespons> call, Throwable t) {

                            }
                        });
            }else {
                mApi.updateReport(activeReport.getId_report(),
                        murid.getId_murid(),
                        user.getIdGuru(),
                        2,
                        etKeterangan.getText().toString(),
                        etWaktu.getText().toString()+":00")
                        .enqueue(new Callback<BaseRespons>() {
                            @Override
                            public void onResponse(Call<BaseRespons> call, Response<BaseRespons> response) {
                                BaseRespons baseRespons = response.body();
                                String message = "";
                                if (baseRespons != null) {
                                    if (!baseRespons.getError()) {
                                        message = "Data berhasil diupdate.";
                                    }
                                }
                                if (response.code() == 400) {
                                    message = "Invalid parameter!";
                                }
                                if (response.code() == 502) {
                                    message = "Gagal menyimpan ke database.";
                                }
                                Toast.makeText(ListKegiatanGuruActivity.this, message, Toast.LENGTH_SHORT).show();
                                dialog.dismiss();
                            }

                            @Override
                            public void onFailure(Call<BaseRespons> call, Throwable t) {

                            }
                        });
            }
            SyncWorker.getSyncWorker().syncReport(getApplicationContext(), mApi.getReport(), false);
            mDialog.dismiss();
        });
        mDialog.show();
    }

    @OnClick(R.id.guru_btnDetailOrtu) void goProfil(){
        Intent intent = new Intent(this, ProfileMuridGuruActivity.class);
        intent.putExtra("id", murid.getId_murid());
        intent.putExtra("id_ortu", murid.getId_ortu());
        startActivity (intent);
    }

    @OnClick(R.id.cv_kegiatanpagi_guru) void goPagi(){
        Intent intent = new Intent(ListKegiatanGuruActivity.this, KegiatanPagiActivity.class);
        intent.putExtra("idKegiatan", 3);
        intent.putExtra("idMurid", murid.getId_murid());
        intent.putExtra("usia", murid.getUsia());
        startActivity(intent);
    }

    @OnClick(R.id.cv_kegiatansiang_guru) void goSiang(){
        Intent intent = new Intent(ListKegiatanGuruActivity.this, KegiatanSiangActivity.class);
        intent.putExtra("idKegiatan", 4);
        intent.putExtra("idMurid", murid.getId_murid());
        startActivity(intent);
    }

    @OnClick(R.id.cv_kegiatansore_guru) void goSore(){
        Intent intent = new Intent(ListKegiatanGuruActivity.this, KegiatanSiangActivity.class);
        intent.putExtra("idKegiatan", 5);
        intent.putExtra("idMurid", murid.getId_murid());
        startActivity(intent);
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void loadReport(List<Report> reportList) {

    }


    @Override
    public void loadMurid(List<Murid> muridList) {

    }
}
