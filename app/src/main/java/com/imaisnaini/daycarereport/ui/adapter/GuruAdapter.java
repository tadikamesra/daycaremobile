package com.imaisnaini.daycarereport.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.imaisnaini.daycarereport.R;
import com.imaisnaini.daycarereport.bl.db.model.Guru;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GuruAdapter extends RecyclerView.Adapter<GuruAdapter.ViewHolder> {
    Context context;
    List<Guru> list = new ArrayList<>();

    public GuruAdapter(Context context) {
        this.context = context;
    }

    public GuruAdapter(Context context, List<Guru> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_guru, parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Guru guru = list.get(position);
        holder.id = guru.getId_guru();
        holder.tvNama.setText(guru.getNama());
        holder.tvEmail.setText(guru.getEmail());
        holder.tvNIP.setText(guru.getNip());
        if (guru.getJk().equals("L")){
            holder.ivAvatar.setImageResource(R.drawable.ic_teacher_man);
        }else {
            holder.ivAvatar.setImageResource(R.drawable.ic_teacher_woman);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.tvNama_guru) TextView tvNama;
        @BindView(R.id.tvEmail_guru) TextView tvEmail;
        @BindView(R.id.tvNIP_guru) TextView tvNIP;
        @BindView(R.id.ivAvatar_guru) ImageView ivAvatar;

        private int id;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void generate(List<Guru> guruList){
        clear();
        list = guruList;
        notifyDataSetChanged();
    }

    public void clear(){
        list.clear();
        notifyDataSetChanged();
    }
}
