package com.imaisnaini.daycarereport.bl.db.dao;

import com.imaisnaini.daycarereport.bl.db.model.Tppa;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.List;

public class TppaDao extends BaseDaoCrud<Tppa, Integer> {
    private static TppaDao tppaDao;

    public static TppaDao getTppaDao(){
        if (tppaDao == null){
            tppaDao = new TppaDao();
        }
        return tppaDao;
    }

    public List<Tppa> getByLingkup(int id) throws SQLException{
        QueryBuilder<Tppa, Integer> qb = getDao().queryBuilder();
        qb.where().eq(Tppa.ID_LINGKUP, id);
        return getDao().query(qb.prepare());
    }
}
