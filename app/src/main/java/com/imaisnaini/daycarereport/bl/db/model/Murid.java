package com.imaisnaini.daycarereport.bl.db.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

@DatabaseTable(tableName = Murid.TBL_NAME)
public class Murid {
    public static final String TBL_NAME = "murid";
    public static final String ID_MURID = "id_murid";
    public static final String NIK = "nik";
    public static final String NAMA = "nama";
    public static final String TGL_LAHIR = "tgl_lahir";
    public static final String JK = "jk";
    public static final String BB = "bb";
    public static final String TB = "tb";
    public static final String ID_ORTU = "id_ortu";
    public static final String USIA = "usia";

    @DatabaseField(columnName = ID_MURID, id = true) private int id_murid;
    @DatabaseField(columnName = NIK) private String nik;
    @DatabaseField(columnName = NAMA) private String nama;
    @DatabaseField(columnName = TGL_LAHIR) private Date tgl_lahir;
    @DatabaseField(columnName = JK) private String jk;
    @DatabaseField(columnName = BB) private int bb;
    @DatabaseField(columnName = TB) private int tb;
    @DatabaseField(columnName = ID_ORTU) private int id_ortu;
    @DatabaseField(columnName = USIA) private int usia;

    public Murid() {
    }

    public int getId_murid() {
        return id_murid;
    }

    public void setId_murid(int id_murid) {
        this.id_murid = id_murid;
    }

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public Date getTgl_lahir() {
        return tgl_lahir;
    }

    public void setTgl_lahir(Date tgl_lahir) {
        this.tgl_lahir = tgl_lahir;
    }

    public String getJk() {
        return jk;
    }

    public void setJk(String jk) {
        this.jk = jk;
    }

    public int getBb() {
        return bb;
    }

    public void setBb(int bb) {
        this.bb = bb;
    }

    public int getTb() {
        return tb;
    }

    public void setTb(int tb) {
        this.tb = tb;
    }

    public int getId_ortu() {
        return id_ortu;
    }

    public void setId_ortu(int id_ortu) {
        this.id_ortu = id_ortu;
    }

    public int getUsia() {
        return usia;
    }

    public void setUsia(int usia) {
        this.usia = usia;
    }
}
