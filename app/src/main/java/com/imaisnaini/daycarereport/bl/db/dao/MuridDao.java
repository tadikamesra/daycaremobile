package com.imaisnaini.daycarereport.bl.db.dao;

import com.imaisnaini.daycarereport.bl.db.model.Murid;
import com.imaisnaini.daycarereport.bl.db.model.Tppa;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.List;

public class MuridDao extends BaseDaoCrud<Murid, Integer>{
    private static MuridDao muridDao;

    public static MuridDao getMuridDao(){
        if (muridDao == null){
            muridDao = new MuridDao();
        }
        return muridDao;
    }

    public Murid getByID(int id) throws SQLException{
        return getDao().queryForId(id);
    }

    public List<Murid> getByOrtu(int id) throws SQLException{
        QueryBuilder<Murid, Integer> qb = getDao().queryBuilder();
        qb.where().eq(Murid.ID_ORTU, id);
        return getDao().query(qb.prepare());
    }
}
