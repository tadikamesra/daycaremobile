package com.imaisnaini.daycarereport.bl.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Gurus {
    @SerializedName("guru")
    @Expose
    private List<Guru> guru = null;

    public List<Guru> getGuru() {
        return guru;
    }

    public void setGuru(List<Guru> guru) {
        this.guru = guru;
    }

    public class Guru {

        @SerializedName("id_guru")
        @Expose
        private int idGuru;
        @SerializedName("nama")
        @Expose
        private String nama;
        @SerializedName("nip")
        @Expose
        private String nip;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("jk")
        @Expose
        private String jk;
        @SerializedName("telp")
        @Expose
        private String telp;
        @SerializedName("password")
        @Expose
        private String password;

        public int getIdGuru() {
            return idGuru;
        }

        public void setIdGuru(int idGuru) {
            this.idGuru = idGuru;
        }

        public String getNama() {
            return nama;
        }

        public void setNama(String nama) {
            this.nama = nama;
        }

        public String getNip() {
            return nip;
        }

        public void setNip(String nip) {
            this.nip = nip;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getJk() {
            return jk;
        }

        public void setJk(String jk) {
            this.jk = jk;
        }

        public String getTelp() {
            return telp;
        }

        public void setTelp(String telp) {
            this.telp = telp;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }
    }
}
