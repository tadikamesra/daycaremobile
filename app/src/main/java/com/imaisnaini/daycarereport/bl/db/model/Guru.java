package com.imaisnaini.daycarereport.bl.db.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = Guru.TBL_NAME)
public class Guru {
    public static final String TBL_NAME = "guru";
    public static final String ID_GURU = "id_guru";
    public static final String NIP = "nip";
    public static final String NAMA = "nama";
    public static final String EMAIL = "email";
    public static final String JK = "jk";
    public static final String TELP = "telp";
    public static final String PASSWORD = "password";

    @DatabaseField(columnName =  ID_GURU, id = true) private int id_guru;
    @DatabaseField(columnName = NIP) private String nip;
    @DatabaseField(columnName = NAMA) private String nama;
    @DatabaseField(columnName = EMAIL) private String email;
    @DatabaseField(columnName = JK) private  String jk;
    @DatabaseField(columnName = TELP) private String telp;
    @DatabaseField(columnName = PASSWORD) private String password;

    public Guru() {
    }

    public int getId_guru() {
        return id_guru;
    }

    public void setId_guru(int id_guru) {
        this.id_guru = id_guru;
    }

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getJk() {
        return jk;
    }

    public void setJk(String jk) {
        this.jk = jk;
    }

    public String getTelp() {
        return telp;
    }

    public void setTelp(String telp) {
        this.telp = telp;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
