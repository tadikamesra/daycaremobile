package com.imaisnaini.daycarereport.bl.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Kegiatans {
    @SerializedName("kegiatan")
    @Expose
    private List<Kegiatan> kegiatan = null;

    public List<Kegiatan> getKegiatan() {
        return kegiatan;
    }

    public void setKegiatan(List<Kegiatan> kegiatan) {
        this.kegiatan = kegiatan;
    }

    public class Kegiatan {

        @SerializedName("id_kegiatan")
        @Expose
        private int idKegiatan;
        @SerializedName("kegiatan")
        @Expose
        private String kegiatan;
        @SerializedName("parent")
        @Expose
        private int parent;

        public int getIdKegiatan() {
            return idKegiatan;
        }

        public void setIdKegiatan(int idKegiatan) {
            this.idKegiatan = idKegiatan;
        }

        public String getKegiatan() {
            return kegiatan;
        }

        public void setKegiatan(String kegiatan) {
            this.kegiatan = kegiatan;
        }

        public int getParent() {
            return parent;
        }

        public void setParent(int parent) {
            this.parent = parent;
        }

    }
}
