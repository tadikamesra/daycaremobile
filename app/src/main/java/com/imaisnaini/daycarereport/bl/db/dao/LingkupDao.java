package com.imaisnaini.daycarereport.bl.db.dao;

import com.imaisnaini.daycarereport.bl.db.model.Lingkup;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class LingkupDao extends BaseDaoCrud<Lingkup, Integer> {
    private static LingkupDao lingkupDao;

    public static LingkupDao getLingkupDao(){
        if (lingkupDao == null){
            lingkupDao = new LingkupDao();
        }
        return lingkupDao;
    }

    public Lingkup getByID(int id) throws SQLException{
        return getDao().queryForId(id);
    }

    public List<Lingkup> getParent() throws SQLException {
        QueryBuilder<Lingkup, Integer> qb = getDao().queryBuilder();
        qb.where().eq(Lingkup.PARENT, 0);
        return getDao().query(qb.prepare());
    }

    public List<Lingkup> getChild(int parent) throws SQLException{
        QueryBuilder<Lingkup, Integer> qb = getDao().queryBuilder();
        qb.where().eq(Lingkup.PARENT, parent);
        return getDao().query(qb.prepare());
    }

}
