package com.imaisnaini.daycarereport.bl.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Lingkups {
    @SerializedName("lingkup")
    @Expose
    private List<Lingkup> lingkup = null;

    public List<Lingkup> getLingkup() {
        return lingkup;
    }

    public void setLingkup(List<Lingkup> lingkup) {
        this.lingkup = lingkup;
    }

    public class Lingkup {

        @SerializedName("id_lingkup")
        @Expose
        private int idLingkup;
        @SerializedName("lingkup")
        @Expose
        private String lingkup;
        @SerializedName("parent")
        @Expose
        private int parent;
        @SerializedName("usia")
        @Expose
        private int usia;
        @SerializedName("interval")
        @Expose
        private int interval;

        public int getIdLingkup() {
            return idLingkup;
        }

        public void setIdLingkup(int idLingkup) {
            this.idLingkup = idLingkup;
        }

        public String getLingkup() {
            return lingkup;
        }

        public void setLingkup(String lingkup) {
            this.lingkup = lingkup;
        }

        public int getParent() {
            return parent;
        }

        public void setParent(int parent) {
            this.parent = parent;
        }

        public int getUsia() {
            return usia;
        }

        public void setUsia(int usia) {
            this.usia = usia;
        }

        public int getInterval() {
            return interval;
        }

        public void setInterval(int interval) {
            this.interval = interval;
        }
    }
}
