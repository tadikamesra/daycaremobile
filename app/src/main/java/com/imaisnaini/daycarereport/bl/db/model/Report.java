package com.imaisnaini.daycarereport.bl.db.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.sql.Date;
import java.sql.Time;

@DatabaseTable(tableName = Report.TBL_NAME)
public class Report {
    public static final String TBL_NAME = "report";
    public static final String ID_REPORT = "id_report";
    public static final String TGL = "tanggal";
    public static final String WAKTU = "waktu";
    public static final String ID_MURID = "id_murid";
    public static final String ID_GURU = "id_guru";
    public static final String ID_TPPA = "id_tppa";
    public static final String ID_KEGIATAN = "id_kegiatan";
    public static final String KETERANGAN = "keterangan";

    @DatabaseField(columnName = ID_REPORT, id = true) private int id_report;
    @DatabaseField(columnName = TGL) private String tanggal;
    @DatabaseField(columnName = WAKTU) private String waktu;
    @DatabaseField(columnName = ID_MURID) private int id_murid;
    @DatabaseField(columnName = ID_GURU) private int id_guru;
    @DatabaseField(columnName = ID_KEGIATAN) private int id_kegiatan;
    @DatabaseField(columnName = ID_TPPA) private int id_tppa;
    @DatabaseField(columnName = KETERANGAN) private String keterangan;

    public Report() {
    }

    public int getId_report() {
        return id_report;
    }

    public void setId_report(int id_report) {
        this.id_report = id_report;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getWaktu() {
        return waktu;
    }

    public void setWaktu(String waktu) {
        this.waktu = waktu;
    }

    public int getId_murid() {
        return id_murid;
    }

    public void setId_murid(int id_murid) {
        this.id_murid = id_murid;
    }

    public int getId_guru() {
        return id_guru;
    }

    public void setId_guru(int id_guru) {
        this.id_guru = id_guru;
    }

    public int getId_kegiatan() {
        return id_kegiatan;
    }

    public void setId_kegiatan(int id_kegiatan) {
        this.id_kegiatan = id_kegiatan;
    }

    public int getId_tppa() {
        return id_tppa;
    }

    public void setId_tppa(int id_tppa) {
        this.id_tppa = id_tppa;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }
}
