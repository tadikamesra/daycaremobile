package com.imaisnaini.daycarereport.bl.network.api;

import android.content.Context;
import android.util.Log;

import com.airbnb.lottie.L;
import com.imaisnaini.daycarereport.bl.db.dao.GuruDao;
import com.imaisnaini.daycarereport.bl.db.dao.KegiatanDao;
import com.imaisnaini.daycarereport.bl.db.dao.LingkupDao;
import com.imaisnaini.daycarereport.bl.db.dao.MuridDao;
import com.imaisnaini.daycarereport.bl.db.dao.OrtuDao;
import com.imaisnaini.daycarereport.bl.db.dao.ReportDao;
import com.imaisnaini.daycarereport.bl.db.dao.TppaDao;
import com.imaisnaini.daycarereport.bl.db.model.Guru;
import com.imaisnaini.daycarereport.bl.db.model.Kegiatan;
import com.imaisnaini.daycarereport.bl.db.model.Lingkup;
import com.imaisnaini.daycarereport.bl.db.model.Murid;
import com.imaisnaini.daycarereport.bl.db.model.Ortu;
import com.imaisnaini.daycarereport.bl.db.model.Report;
import com.imaisnaini.daycarereport.bl.db.model.Tppa;
import com.imaisnaini.daycarereport.bl.network.config.Config;
import com.imaisnaini.daycarereport.bl.network.model.Gurus;
import com.imaisnaini.daycarereport.bl.network.model.Kegiatans;
import com.imaisnaini.daycarereport.bl.network.model.Lingkups;
import com.imaisnaini.daycarereport.bl.network.model.Murids;
import com.imaisnaini.daycarereport.bl.network.model.Ortus;
import com.imaisnaini.daycarereport.bl.network.model.Reports;
import com.imaisnaini.daycarereport.bl.network.model.Tppas;
import com.imaisnaini.daycarereport.ui.dialog.DialogBuilder;

import org.threeten.bp.LocalDate;
import org.threeten.bp.Period;

import java.sql.Date;
import java.sql.SQLException;
import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SyncWorker {
    private static SyncWorker syncWorker;

    public static SyncWorker getSyncWorker() {
        if (syncWorker == null) {
            syncWorker = new SyncWorker();
        }
        return syncWorker;
    }

    public void syncMurid(final Context context, Call<Murids> muridsCall, final boolean isFirstRun){
        muridsCall.enqueue(new Callback<Murids>() {
            @Override
            public void onResponse(Call<Murids> call, Response<Murids> response) {
                if (response.isSuccessful()){
                    Murids murids = response.body();
                    Log.i("MURID_GET", response.message());
                    for (Murids.Murid list: murids.getMurid()) {
                        Murid murid = new Murid();
                        LocalDate birth = null;
                        birth = LocalDate.of(list.getTglLahir().getYear() + 1900, list.getTglLahir().getMonth(), list.getTglLahir().getDate());
                        int year =  Period.between(birth, LocalDate.now()).getYears();
                        int month = Period.between(birth, LocalDate.now()).getMonths();
                        int totalBulan = year * 12 + month;

                        murid.setId_murid(list.getIdMurid());
                        murid.setNama(list.getNama());
                        murid.setNik(list.getNik());
                        murid.setTgl_lahir(list.getTglLahir());
                        murid.setJk(list.getJk());
                        murid.setBb(list.getBb());
                        murid.setTb(list.getTb());
                        murid.setId_ortu(list.getIdOrtu());
                        murid.setUsia(totalBulan);

                        try {
                            if (isFirstRun){
                                MuridDao.getMuridDao().add(murid);
                            }else {
                                MuridDao.getMuridDao().save(murid);
                            }
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<Murids> call, Throwable t) {
                DialogBuilder.showErrorDialog(context, t.getMessage());
                Log.i("MURID_GET", t.getMessage());
            }
        });
    }

    public void syncOrtu(final Context context, Call<Ortus> ortusCall, final boolean isFirstRun){
        ortusCall.enqueue(new Callback<Ortus>() {
            @Override
            public void onResponse(Call<Ortus> call, Response<Ortus> response) {
                if (response.isSuccessful()){
                    Ortus ortus = response.body();
                    Log.i("ORTU_GET", response.message());
                    for (Ortus.Ortu list : ortus.getOrtu()) {
                        Ortu ortu = new Ortu();
                        ortu.setId_ortu(list.getIdOrtu());
                        ortu.setNama(list.getNama());
                        ortu.setNik(list.getNik());
                        ortu.setJk(list.getJk());
                        ortu.setTelp(list.getTelp());
                        ortu.setAlamat(list.getAlamat());

                        try {
                            if (isFirstRun){
                                OrtuDao.getOrtuDao().add(ortu);
                            }else {
                                OrtuDao.getOrtuDao().save(ortu);
                            }
                        }catch (SQLException e){
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<Ortus> call, Throwable t) {
                DialogBuilder.showErrorDialog(context, t.getMessage());
                Log.i("ORTU_GET", t.getMessage());
            }
        });
    }

    public void syncGuru(final Context context, Call<Gurus> gurusCall, final boolean isFirstRun){
        gurusCall.enqueue(new Callback<Gurus>() {
            @Override
            public void onResponse(Call<Gurus> call, Response<Gurus> response) {
                if (response.isSuccessful()){
                    Gurus gurus = response.body();
                    Log.i("GURU_GET", response.message());
                    for (Gurus.Guru list : gurus.getGuru()) {
                        Guru guru = new Guru();
                        guru.setEmail(list.getEmail());
                        guru.setId_guru(list.getIdGuru());
                        guru.setNama(list.getNama());
                        guru.setNip(list.getNip());
                        guru.setTelp(list.getTelp());
                        guru.setPassword(list.getPassword());
                        guru.setJk(list.getJk());

                        try {
                            if (isFirstRun) {
                                GuruDao.getGuruDao().add(guru);
                            } else {
                                GuruDao.getGuruDao().save(guru);
                            }
                        }catch (SQLException e){
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<Gurus> call, Throwable t) {
                DialogBuilder.showErrorDialog(context, t.getMessage());
                Log.i("GURU_GET", t.getMessage());
            }
        });
    }

    public void syncKegiatan(final Context context, Call<Kegiatans> kegiatansCall, final boolean isFirstRun){
        kegiatansCall.enqueue(new Callback<Kegiatans>() {
            @Override
            public void onResponse(Call<Kegiatans> call, Response<Kegiatans> response) {
                if (response.isSuccessful()){
                    Log.i("KEGIATAN_GET", response.message());
                    Kegiatans list = response.body();
                    for (Kegiatans.Kegiatan kegiatan : list.getKegiatan()) {
                        Kegiatan obj = new Kegiatan();
                        obj.setId_kegiatan(kegiatan.getIdKegiatan());
                        obj.setKegetian(kegiatan.getKegiatan());
                        obj.setParent(kegiatan.getParent());

                        try {
                            if (isFirstRun){
                                KegiatanDao.getKegiatanDao().add(obj);
                            }else {
                                KegiatanDao.getKegiatanDao().save(obj);
                            }
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<Kegiatans> call, Throwable t) {
                DialogBuilder.showErrorDialog(context, t.getMessage());
                Log.i("KEGIATAN_GET", t.getMessage());
            }
        });
    }

    public void syncTppa(final Context context, Call<Tppas> tppasCall, final boolean isFirstRun){
        tppasCall.enqueue(new Callback<Tppas>() {
            @Override
            public void onResponse(Call<Tppas> call, Response<Tppas> response) {
                if (response.isSuccessful()){
                    Log.i("TPPA_GET", response.message());
                    Tppas list = response.body();
                    for (Tppas.Tppa tppa : list.getTppa()) {
                        Tppa obj = new Tppa();
                        obj.setId_tppa(tppa.getIdTppa());
                        obj.setId_lingkup(tppa.getIdLingkup());
                        obj.setUsia(tppa.getUsia());
                        obj.setInterval(tppa.getInterval());
                        obj.setKeterangan(tppa.getKeterangan());

                        try {
                            if (isFirstRun){
                                TppaDao.getTppaDao().add(obj);
                            }else {
                                TppaDao.getTppaDao().save(obj);
                            }
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<Tppas> call, Throwable t) {
                DialogBuilder.showErrorDialog(context, t.getMessage());
                Log.i("TPPA_GET", t.getMessage());
            }
        });
    }

    public void syncLingkup(final Context context, Call<Lingkups> lingkupsCall, final boolean isFirstRun){
        lingkupsCall.enqueue(new Callback<Lingkups>() {
            @Override
            public void onResponse(Call<Lingkups> call, Response<Lingkups> response) {
                if (response.isSuccessful()){
                    Lingkups list = response.body();
                    Log.i("LINGKUP_GET", response.message());
                    for (Lingkups.Lingkup lingkup : list.getLingkup()) {
                        Lingkup obj = new Lingkup();
                        obj.setId_lingkup(lingkup.getIdLingkup());
                        obj.setLingkup(lingkup.getLingkup());
                        obj.setParent(lingkup.getParent());
                        obj.setUsia(lingkup.getUsia());
                        obj.setInterval(lingkup.getInterval());

                        try {
                            if (isFirstRun){
                                LingkupDao.getLingkupDao().add(obj);
                            }else {
                                LingkupDao.getLingkupDao().save(obj);
                            }
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<Lingkups> call, Throwable t) {
                DialogBuilder.showErrorDialog(context, t.getMessage());
                Log.i("LINGKUP_GET", t.getMessage());
            }
        });
    }

    public void syncReport(final Context context, Call<Reports> reportsCall, final boolean isFirstRun){
        reportsCall.enqueue(new Callback<Reports>() {
            @Override
            public void onResponse(Call<Reports> call, Response<Reports> response) {
                if (response.isSuccessful()){
                    Reports list = response.body();
                    Log.i("REPORT_GET", response.message());
                    for (Reports.Report report : list.getReport()){
                        Report obj = new Report();
                        obj.setId_report(report.getIdReport());
                        obj.setId_guru(report.getIdGuru());
                        obj.setId_murid(report.getIdMurid());
                        obj.setId_kegiatan(report.getIdKegiatan());
                        obj.setKeterangan(report.getKeterangan());
                        obj.setId_tppa(report.getIdTppa());
                        obj.setTanggal(report.getTanggal());
                        obj.setWaktu(report.getWaktu());

                        try {
                            if (isFirstRun){
                                ReportDao.getReportDao().add(obj);
                            }else {
                                ReportDao.getReportDao().save(obj);
                            }
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<Reports> call, Throwable t) {
                DialogBuilder.showErrorDialog(context, t.getMessage());
                Log.i("REPORT_GET", t.getMessage());
            }
        });
    }
}
