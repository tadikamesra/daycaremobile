package com.imaisnaini.daycarereport.bl.db.dao;

import com.imaisnaini.daycarereport.bl.db.model.Lingkup;
import com.imaisnaini.daycarereport.bl.db.model.Ortu;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.List;

public class OrtuDao extends BaseDaoCrud<Ortu, Integer> {
    private static OrtuDao ortuDao;

    public static OrtuDao getOrtuDao(){
        if (ortuDao == null){
            ortuDao = new OrtuDao();
        }
        return ortuDao;
    }

    public Ortu getByID(int id) throws SQLException{
        return getDao().queryForId(id);
    }
}
