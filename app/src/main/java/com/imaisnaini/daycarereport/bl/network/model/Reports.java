package com.imaisnaini.daycarereport.bl.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.sql.Date;
import java.sql.Time;
import java.util.List;

public class Reports {
    @SerializedName("report")
    @Expose
    private List<Report> report = null;

    public List<Report> getReport() {
        return report;
    }

    public void setReport(List<Report> report) {
        this.report = report;
    }

    public class Report {

        @SerializedName("id_report")
        @Expose
        private int idReport;
        @SerializedName("tanggal")
        @Expose
        private String tanggal;
        @SerializedName("waktu")
        @Expose
        private String waktu;
        @SerializedName("id_murid")
        @Expose
        private int idMurid;
        @SerializedName("id_guru")
        @Expose
        private int idGuru;
        @SerializedName("id_kegiatan")
        @Expose
        private int idKegiatan;
        @SerializedName("id_tppa")
        @Expose
        private int idTppa;
        @SerializedName("keterangan")
        @Expose
        private String keterangan;

        public int getIdReport() {
            return idReport;
        }

        public void setIdReport(int idReport) {
            this.idReport = idReport;
        }

        public int getIdMurid() {
            return idMurid;
        }

        public void setIdMurid(int idMurid) {
            this.idMurid = idMurid;
        }

        public int getIdGuru() {
            return idGuru;
        }

        public void setIdGuru(int idGuru) {
            this.idGuru = idGuru;
        }

        public String getTanggal() {
            return tanggal;
        }

        public void setTanggal(String tanggal) {
            this.tanggal = tanggal;
        }

        public String getWaktu() {
            return waktu;
        }

        public void setWaktu(String waktu) {
            this.waktu = waktu;
        }

        public int getIdKegiatan() {
            return idKegiatan;
        }

        public void setIdKegiatan(int idKegiatan) {
            this.idKegiatan = idKegiatan;
        }

        public int getIdTppa() {
            return idTppa;
        }

        public void setIdTppa(int idTppa) {
            this.idTppa = idTppa;
        }

        public String getKeterangan() {
            return keterangan;
        }

        public void setKeterangan(String keterangan) {
            this.keterangan = keterangan;
        }
    }
}
