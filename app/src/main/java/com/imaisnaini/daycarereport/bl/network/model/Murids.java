package com.imaisnaini.daycarereport.bl.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

public class Murids {
    @SerializedName("murid")
    @Expose
    private List<Murid> murid = null;

    public List<Murid> getMurid() {
        return murid;
    }

    public void setMurid(List<Murid> murid) {
        this.murid = murid;
    }

    public class Murid {

        @SerializedName("id_murid")
        @Expose
        private int idMurid;
        @SerializedName("nik")
        @Expose
        private String nik;
        @SerializedName("nama")
        @Expose
        private String nama;
        @SerializedName("tgl_lahir")
        @Expose
        private Date tglLahir;
        @SerializedName("jk")
        @Expose
        private String jk;
        @SerializedName("bb")
        @Expose
        private int bb;
        @SerializedName("tb")
        @Expose
        private int tb;
        @SerializedName("id_ortu")
        @Expose
        private int idOrtu;

        public int getIdMurid() {
            return idMurid;
        }

        public void setIdMurid(int idMurid) {
            this.idMurid = idMurid;
        }

        public String getNik() {
            return nik;
        }

        public void setNik(String nik) {
            this.nik = nik;
        }

        public String getNama() {
            return nama;
        }

        public void setNama(String nama) {
            this.nama = nama;
        }

        public Date getTglLahir() {
            return tglLahir;
        }

        public void setTglLahir(Date tglLahir) {
            this.tglLahir = tglLahir;
        }

        public String getJk() {
            return jk;
        }

        public void setJk(String jk) {
            this.jk = jk;
        }

        public int getBb() {
            return bb;
        }

        public void setBb(int bb) {
            this.bb = bb;
        }

        public int getTb() {
            return tb;
        }

        public void setTb(int tb) {
            this.tb = tb;
        }

        public int getIdOrtu() {
            return idOrtu;
        }

        public void setIdOrtu(int idOrtu) {
            this.idOrtu = idOrtu;
        }
    }
}
