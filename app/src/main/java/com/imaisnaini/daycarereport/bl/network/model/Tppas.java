package com.imaisnaini.daycarereport.bl.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Tppas {
    @SerializedName("tppa")
    @Expose
    private List<Tppa> tppa = null;

    public List<Tppa> getTppa() {
        return tppa;
    }

    public void setTppa(List<Tppa> tppa) {
        this.tppa = tppa;
    }

    public class Tppa {

        @SerializedName("id_tppa")
        @Expose
        private int idTppa;
        @SerializedName("usia")
        @Expose
        private int usia;
        @SerializedName("interval")
        @Expose
        private int interval;
        @SerializedName("keterangan")
        @Expose
        private String keterangan;
        @SerializedName("id_lingkup")
        @Expose
        private int idLingkup;

        public int getIdTppa() {
            return idTppa;
        }

        public void setIdTppa(int idTppa) {
            this.idTppa = idTppa;
        }

        public int getUsia() {
            return usia;
        }

        public void setUsia(int usia) {
            this.usia = usia;
        }

        public int getInterval() {
            return interval;
        }

        public void setInterval(int interval) {
            this.interval = interval;
        }

        public String getKeterangan() {
            return keterangan;
        }

        public void setKeterangan(String keterangan) {
            this.keterangan = keterangan;
        }

        public int getIdLingkup() {
            return idLingkup;
        }

        public void setIdLingkup(int idLingkup) {
            this.idLingkup = idLingkup;
        }

    }
}
