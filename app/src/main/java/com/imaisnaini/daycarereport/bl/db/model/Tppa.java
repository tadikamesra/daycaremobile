package com.imaisnaini.daycarereport.bl.db.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = Tppa.TBL_NAME)
public class Tppa {
    public static final String TBL_NAME = "tppa";
    public static final String ID_TPPA = "id_tppa";
    public static final String USIA = "usia";
    public static final String INTERVAL = "interval";
    public static final String KETERAGAN = "keterangan";
    public static final String ID_LINGKUP = "id_lingkup";

    @DatabaseField(columnName = ID_TPPA, id = true) private int id_tppa;
    @DatabaseField(columnName = USIA) private int usia;
    @DatabaseField(columnName = INTERVAL) private int interval;
    @DatabaseField(columnName = KETERAGAN) private String keterangan;
    @DatabaseField(columnName = ID_LINGKUP) private int id_lingkup;

    public Tppa() {
    }

    public int getId_tppa() {
        return id_tppa;
    }

    public void setId_tppa(int id_tppa) {
        this.id_tppa = id_tppa;
    }

    public int getUsia() {
        return usia;
    }

    public void setUsia(int usia) {
        this.usia = usia;
    }

    public int getInterval() {
        return interval;
    }

    public void setInterval(int interval) {
        this.interval = interval;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public int getId_lingkup() {
        return id_lingkup;
    }

    public void setId_lingkup(int id_lingkup) {
        this.id_lingkup = id_lingkup;
    }
}
