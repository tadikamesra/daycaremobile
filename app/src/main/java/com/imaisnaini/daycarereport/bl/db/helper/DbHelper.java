package com.imaisnaini.daycarereport.bl.db.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

import com.imaisnaini.daycarereport.bl.db.model.Guru;
import com.imaisnaini.daycarereport.bl.db.model.Kegiatan;
import com.imaisnaini.daycarereport.bl.db.model.Lingkup;
import com.imaisnaini.daycarereport.bl.db.model.Murid;
import com.imaisnaini.daycarereport.bl.db.model.Ortu;
import com.imaisnaini.daycarereport.bl.db.model.Report;
import com.imaisnaini.daycarereport.bl.db.model.Tppa;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

public class DbHelper extends OrmLiteSqliteOpenHelper {
    private static final int DBVER = 1;
    public static final String DBNAME = "daycare.db";

    public DbHelper(Context ctx){
        super(ctx, DBNAME, null, DBVER);
    }
    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, Murid.class);
            TableUtils.createTable(connectionSource, Guru.class);
            TableUtils.createTable(connectionSource, Ortu.class);
            TableUtils.createTable(connectionSource, Kegiatan.class);
            TableUtils.createTable(connectionSource, Lingkup.class);
            TableUtils.createTable(connectionSource, Tppa.class);
            TableUtils.createTable(connectionSource, Report.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {}

    @Override
    public ConnectionSource getConnectionSource() {
        return super.getConnectionSource();
    }

}