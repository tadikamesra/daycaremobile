package com.imaisnaini.daycarereport.bl.db.dao;

import com.imaisnaini.daycarereport.bl.db.model.Kegiatan;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.List;

public class KegiatanDao extends BaseDaoCrud<Kegiatan, Integer> {
    private static KegiatanDao kegiatanDao;

    public static KegiatanDao getKegiatanDao(){
        if (kegiatanDao == null){
            kegiatanDao = new KegiatanDao();
        }
        return kegiatanDao;
    }

    public List<Kegiatan> getByParent(int parent) throws SQLException{
        QueryBuilder<Kegiatan, Integer> qb = getDao().queryBuilder();
        qb.where().eq(Kegiatan.PARENT, parent);
        return getDao().query(qb.prepare());
    }
}
