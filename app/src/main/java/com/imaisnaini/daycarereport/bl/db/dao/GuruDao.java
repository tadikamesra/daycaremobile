package com.imaisnaini.daycarereport.bl.db.dao;

import com.imaisnaini.daycarereport.bl.db.model.Guru;

import java.sql.SQLException;

public class GuruDao extends BaseDaoCrud<Guru, Integer> {
    private static GuruDao guruDao;

    public static GuruDao getGuruDao(){
        if (guruDao==null){
            guruDao = new GuruDao();
        }
        return guruDao;
    }

    public Guru getByID(int id) throws SQLException {
        return getDao().queryForId(id);
    }
}
