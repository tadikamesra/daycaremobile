package com.imaisnaini.daycarereport.bl.network.config;

public class Config {
    public static final String BASE_URL = "https://daycarereport.000webhostapp.com/";
    //public static final String BASE_URL = "http://192.168.43.191/daycarewebserver/";

    public static final String API_LOGIN_GURU = BASE_URL + "guru/login.php";
    public static final String API_LOGIN_ORTU = BASE_URL + "ortu/login.php";
    public static final String API_READ_GURU = BASE_URL + "guru/list.php";
    public static final String API_READ_MURID = BASE_URL + "murid/list.php";
    public static final String API_UPDATE_GURU = BASE_URL + "guru/update.php";
    public static final String API_UPDATE_MURID = BASE_URL + "murid/update.php";
    public static final String API_READ_TPPA = BASE_URL + "tppa/read.php";
    public static final String API_READ_KEGIATAN = BASE_URL +"kegiatan/read.php";
    public static final String API_READ_LINGKUP = BASE_URL + "lingkup/read.php";
    public static final String API_READ_REPORT = BASE_URL + "report/read.php";
    public static final String API_UPDATE_REPORT = BASE_URL + "report/update.php";
    public static final String API_ADD_REPORT = BASE_URL + "report/add.php";
    public static final String API_READ_ORTU = BASE_URL + "ortu/read.php";
    public static final String API_ADD_ORTU = BASE_URL + "ortu/add.php";
    public static final String API_UPDATE_ORTU = BASE_URL + "ortu/update.php";
    public static final String API_GET_TPPA = BASE_URL + "tppa/getTppa.php";
    public static final String API_ADD_MURID = BASE_URL + "murid/add.php";
    public static final String API_ADD_GURU = BASE_URL + "guru/add.php";
}
