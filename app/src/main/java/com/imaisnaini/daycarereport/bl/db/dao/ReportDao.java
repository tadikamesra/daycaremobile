package com.imaisnaini.daycarereport.bl.db.dao;

import com.imaisnaini.daycarereport.bl.db.model.Report;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.Date;
import java.sql.SQLException;
import java.util.List;

public class ReportDao extends BaseDaoCrud<Report, Integer> {
    private static ReportDao reportDao;

    public static ReportDao getReportDao(){
        if (reportDao == null){
            reportDao = new ReportDao();
        }
        return reportDao;
    }

    public Report getByID(int id) throws SQLException{
        return getDao().queryForId(id);
    }

    public List<Report> getByMurid(int id_murid) throws SQLException{
        QueryBuilder<Report, Integer> qb = getDao().queryBuilder();
        qb.where().eq(Report.ID_MURID, id_murid);
        return getDao().query(qb.prepare());
    }

    public List<Report> getByTgl(String tgl, int id_murid) throws SQLException{
        QueryBuilder<Report, Integer> qb = getDao().queryBuilder();
        qb.where().eq(Report.TGL, tgl)
                .and().eq(Report.ID_MURID, id_murid);
        return getDao().query(qb.prepare());
    }
}
