package com.imaisnaini.daycarereport.bl.db.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = Kegiatan.TBL_NAME)
public class Kegiatan {
    public static final String TBL_NAME = "kegiatan";
    public static final String ID_KEGIATAN = "id_kegiatan";
    public static final String KEGIATAN = "kegiatan";
    public static final String PARENT = "parent";

    @DatabaseField(columnName = ID_KEGIATAN, id = true) private int id_kegiatan;
    @DatabaseField(columnName = KEGIATAN) private String kegetian;
    @DatabaseField(columnName = PARENT) private int parent;

    public Kegiatan() {
    }

    public int getId_kegiatan() {
        return id_kegiatan;
    }

    public void setId_kegiatan(int id_kegiatan) {
        this.id_kegiatan = id_kegiatan;
    }

    public String getKegetian() {
        return kegetian;
    }

    public void setKegetian(String kegetian) {
        this.kegetian = kegetian;
    }

    public int getParent() {
        return parent;
    }

    public void setParent(int parent) {
        this.parent = parent;
    }
}
