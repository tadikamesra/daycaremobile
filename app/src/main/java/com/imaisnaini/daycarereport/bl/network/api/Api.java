package com.imaisnaini.daycarereport.bl.network.api;

import android.text.format.Time;

import com.imaisnaini.daycarereport.bl.network.config.Config;
import com.imaisnaini.daycarereport.bl.network.model.BaseRespons;
import com.imaisnaini.daycarereport.bl.network.model.Gurus;
import com.imaisnaini.daycarereport.bl.network.model.Kegiatans;
import com.imaisnaini.daycarereport.bl.network.model.Lingkups;
import com.imaisnaini.daycarereport.bl.network.model.Murids;
import com.imaisnaini.daycarereport.bl.network.model.Ortus;
import com.imaisnaini.daycarereport.bl.network.model.Reports;
import com.imaisnaini.daycarereport.bl.network.model.Tppas;
import com.imaisnaini.daycarereport.bl.network.model.UserGuru;
import com.imaisnaini.daycarereport.bl.network.model.UserOrtu;

import java.util.Date;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface Api {
    @FormUrlEncoded
    @POST(Config.API_LOGIN_GURU)
    Call<UserGuru> loginGuru(
            @Field("email") String email,
            @Field("password") String password
    );

    @FormUrlEncoded
    @POST(Config.API_LOGIN_ORTU)
    Call<UserOrtu> loginOrtu(
            @Field("nik") String nik,
            @Field("telp") String telp
    );

    @GET(Config.API_READ_GURU)
    Call<Gurus> getGuru();

    @GET(Config.API_READ_MURID)
    Call<Murids> getMurid();

    @GET(Config.API_READ_ORTU)
    Call<Ortus> getOrtu();

    @GET(Config.API_READ_KEGIATAN)
    Call<Kegiatans> getKegiatan();

    @GET(Config.API_READ_LINGKUP)
    Call<Lingkups> getLingkup();

    @GET(Config.API_READ_TPPA)
    Call<Tppas> getTppa();

    @GET(Config.API_READ_REPORT)
    Call<Reports> getReport();

    @FormUrlEncoded
    @POST(Config.API_ADD_REPORT)
    Call<BaseRespons> addReport(
            @Field("id_murid") int id_murid,
            @Field("id_guru") int id_guru,
            @Field("id_kegiatan") int id_kegiatan,
            @Field("keterangan") String keterangan,
            @Field("waktu") String waktu
            );

    @FormUrlEncoded
    @POST(Config.API_ADD_REPORT)
    Call<BaseRespons> addReport(
            @Field("id_murid") int id_murid,
            @Field("id_guru") int id_guru,
            @Field("id_kegiatan") int id_kegiatan,
            @Field("keterangan") String keterangan,
            @Field("waktu") String waktu ,
            @Field("id_tppa") int id_tppa
    );

    @FormUrlEncoded
    @POST(Config.API_UPDATE_REPORT)
    Call<BaseRespons> updateReport(
            @Field("id_report") int id_report,
            @Field("id_murid") int id_murid,
            @Field("id_guru") int id_guru,
            @Field("id_kegiatan") int id_kegiatan,
            @Field("keterangan") String keterangan,
            @Field("waktu") String waktu
    );

    @FormUrlEncoded
    @POST(Config.API_UPDATE_REPORT)
    Call<BaseRespons> updateReport(
            @Field("id_report") int id_report,
            @Field("id_murid") int id_murid,
            @Field("id_guru") int id_guru,
            @Field("id_kegiatan") int id_kegiatan,
            @Field("keterangan") String keterangan,
            @Field("waktu") String waktu ,
            @Field("id_tppa") int id_tppa
    );

    @FormUrlEncoded
    @POST(Config.API_UPDATE_GURU)
    Call<BaseRespons> updateGuru(
            @Field("id_guru") int id_guru,
            @Field("nama") String nama,
            @Field("nip") String nip,
            @Field("email") String email,
            @Field("jk") String jk ,
            @Field("telp") String telp
    );

    @FormUrlEncoded
    @POST(Config.API_UPDATE_ORTU)
    Call<BaseRespons> updateOrtu(
            @Field("id_ortu") int id_ortu,
            @Field("nama") String nama,
            @Field("nik") String nik,
            @Field("jk") String jk ,
            @Field("telp") String telp,
            @Field("alamat") String alamat
    );

    @FormUrlEncoded
    @POST(Config.API_ADD_ORTU)
    Call<BaseRespons> addOrtu(
            @Field("nama") String nama,
            @Field("nik") String nik,
            @Field("jk") String jk ,
            @Field("telp") String telp,
            @Field("alamat") String alamat
    );

    @FormUrlEncoded
    @POST(Config.API_GET_TPPA)
    Call<Tppas> getTPPA(
            @Field("id_lingkup") int id_lingkup,
            @Field("usia") int usia
    );

    @FormUrlEncoded
    @POST(Config.API_ADD_MURID)
    Call<BaseRespons> addMurid(
            @Field("nama")      String nama,
            @Field("nik")       String nik,
            @Field("tgl_lahir") String tgl_lahir,
            @Field("jk")        String jk,
            @Field("bb")        String bb,
            @Field("tb")        String tb,
            @Field("id_ortu")   int id_ortu
    );

    @FormUrlEncoded
    @POST (Config.API_ADD_GURU)
    Call<BaseRespons> addGuru(
            @Field("nama")      String nama,
            @Field("nip")       String nip,
            @Field("email")     String email,
            @Field("jk")        String jk,
            @Field("telp")      String telp,
            @Field("password")  String password
    );

}
