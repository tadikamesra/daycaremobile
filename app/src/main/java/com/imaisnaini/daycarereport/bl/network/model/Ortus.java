package com.imaisnaini.daycarereport.bl.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Ortus {
    @SerializedName("ortu")
    @Expose
    private List<Ortu> ortu = null;

    public List<Ortu> getOrtu() {
        return ortu;
    }

    public void setOrtu(List<Ortu> ortu) {
        this.ortu = ortu;
    }

    public class Ortu {

        @SerializedName("id_ortu")
        @Expose
        private int idOrtu;
        @SerializedName("nama")
        @Expose
        private String nama;
        @SerializedName("nik")
        @Expose
        private String nik;
        @SerializedName("jk")
        @Expose
        private String jk;
        @SerializedName("telp")
        @Expose
        private String telp;
        @SerializedName("alamat")
        @Expose
        private String alamat;

        public int getIdOrtu() {
            return idOrtu;
        }

        public void setIdOrtu(int idOrtu) {
            this.idOrtu = idOrtu;
        }

        public String getNama() {
            return nama;
        }

        public void setNama(String nama) {
            this.nama = nama;
        }

        public String getNik() {
            return nik;
        }

        public void setNik(String nik) {
            this.nik = nik;
        }

        public String getJk() {
            return jk;
        }

        public void setJk(String jk) {
            this.jk = jk;
        }

        public String getTelp() {
            return telp;
        }

        public void setTelp(String telp) {
            this.telp = telp;
        }

        public String getAlamat() {
            return alamat;
        }

        public void setAlamat(String alamat) {
            this.alamat = alamat;
        }

    }
}
