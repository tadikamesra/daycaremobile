package com.imaisnaini.daycarereport.bl.db.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

@DatabaseTable(tableName = Ortu.TBL_NAME)
public class Ortu {
    public static final String TBL_NAME = "orangtua";
    public static final String ID_ORTU = "id_ortu";
    public static final String NIK = "nik";
    public static final String NAMA = "nama";
    public static final String JK = "jk";
    public static final String TELP = "telp";
    public static final String ALAMAT = "alamat";

    @DatabaseField(columnName = ID_ORTU, id = true) private int id_ortu;
    @DatabaseField(columnName = NIK) private String nik;
    @DatabaseField(columnName = NAMA) private String nama;
    @DatabaseField(columnName = JK) private String jk;
    @DatabaseField(columnName = TELP) private String telp;
    @DatabaseField(columnName = ALAMAT) private String alamat;

    public Ortu() {}

    public int getId_ortu() { return id_ortu; }

    public void setId_ortu(int id_ortu) {
        this.id_ortu = id_ortu;
    }

    public String getNik() { return nik; }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public String getNama() { return nama; }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getJk() { return jk; }

    public void setJk(String jk) {
        this.jk = jk;
    }

    public String getTelp() { return telp; }

    public void setTelp(String telp) {
        this.telp = telp;
    }

    public String getAlamat() { return alamat; }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }
}
