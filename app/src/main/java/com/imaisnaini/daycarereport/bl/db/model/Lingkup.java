package com.imaisnaini.daycarereport.bl.db.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = Lingkup.TBL_NAME)
public class Lingkup {
    public static final String TBL_NAME = "lingkup";
    public static final String ID_LINGKUP = "id_lingkup";
    public static final String LINGKUP = "lingkup";
    public static final String PARENT = "parent";
    public static final String USIA = "usia";
    public static final String INTERVAL = "interval";

    @DatabaseField(columnName = ID_LINGKUP, id = true) private int id_lingkup;
    @DatabaseField(columnName = LINGKUP) private String lingkup;
    @DatabaseField(columnName = PARENT) private int parent;
    @DatabaseField(columnName = USIA) private int usia;
    @DatabaseField(columnName = INTERVAL) private int interval;

    public Lingkup() {
    }

    public int getId_lingkup() {
        return id_lingkup;
    }

    public void setId_lingkup(int id_lingkup) {
        this.id_lingkup = id_lingkup;
    }

    public String getLingkup() {
        return lingkup;
    }

    public void setLingkup(String lingkup) {
        this.lingkup = lingkup;
    }

    public int getParent() {
        return parent;
    }

    public void setParent(int parent) {
        this.parent = parent;
    }

    public int getUsia() {
        return usia;
    }

    public void setUsia(int usia) {
        this.usia = usia;
    }

    public int getInterval() {
        return interval;
    }

    public void setInterval(int interval) {
        this.interval = interval;
    }
}
